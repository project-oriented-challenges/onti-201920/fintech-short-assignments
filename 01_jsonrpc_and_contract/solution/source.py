import json
import rlp
import eth_account.account
import eth_utils.crypto
import os

for i in range(1 if 'ONLY_ONE' in os.environ else 13):
    data = bytes.fromhex(input())

    offset = 0

    contracts = {}
    pending_requests = {}
    states = {}

    def processRequest(srcIp, reqBody):
        method = reqBody['method']
        params = reqBody['params']

        if srcIp not in states:
            states[srcIp] = {
                'deploy': '',
                'salted': False,
                'invokeMe': False,
                'setSalted': False,
                'error': False
            }

        if method == 'eth_sendRawTransaction':
            rawTx = params[0][2:]
            decoded = rlp.decode(bytes.fromhex(rawTx))
            data = decoded[5].hex()
            nonce = int.from_bytes(decoded[0], byteorder='big')
            sender = eth_account.Account.recoverTransaction(rawTx).lower()
            to = '0x' + decoded[3].hex()
            value = decoded[4].hex()
            gas = int.from_bytes(decoded[2], byteorder='big')
            if len(data) > 800 and gas >= 183410:
                contract = '0x' + eth_utils.crypto.keccak(rlp.encode([bytes.fromhex(sender[2:]), nonce])).hex()[-40:]
                contracts[contract] = {
                    'allowance': False,
                    'creator': sender
                }
                states[srcIp]['deploy'] = contract
            elif data == 'e42ab25d0000000000000000000000000000000000000000000000000000000000000001' and value == '' and gas >= 27000:
                if to in contracts and contracts[to]['creator'] == sender:
                    states[srcIp]['invokeMe'] = True
                    contracts[to]['allowance'] = True
                else:
                    states[srcIp]['error'] = True
            elif data == 'e42ab25d0000000000000000000000000000000000000000000000000000000000000000' and value == '' and gas >= 27000:
                if to in contracts and contracts[to]['creator'] == sender:
                    states[srcIp]['invokeMe'] = True
                    contracts[to]['allowance'] = False
                else:
                    states[srcIp]['error'] = True
            elif data == '23a48c6c' and gas >= 45000 and value == '':
                if to in contracts and (contracts[to]['creator'] == sender or contracts[to]['allowance']):
                    states[srcIp]['setSalted'] = True
                else:
                    states[srcIp]['error'] = True
            else:
                states[srcIp]['error'] = True
        elif method == 'eth_call':
            if params[0]['to'] == states[srcIp]['deploy'] and params[0]['data'] == '0x1f981516':
                states[srcIp]['salted'] = True
            else:
                states[srcIp]['error'] = True


    while offset < len(data):
        size = data[offset + 16] * 256 + data[offset + 17] + 14
        packet = data[offset:offset + size]
        srcIp = int.from_bytes(packet[26:30], byteorder='big')
        destIp = int.from_bytes(packet[30:34], byteorder='big')
        if packet[66:70] == bytes('POST', 'utf-8'):
            body = json.loads(packet[4 + packet.find(b'\r\n\r\n'):].decode('utf-8'))
            processRequest(srcIp, body)
        offset += size

    for ip in states:
        if states[ip]['deploy'] != '' and states[ip]['salted'] and states[ip]['setSalted'] and states[ip]['invokeMe'] and not states[ip]['error']:
            bs = ip.to_bytes(4, byteorder='big')
            print('%d.%d.%d.%d' % (bs[0], bs[1], bs[2], bs[3]))
