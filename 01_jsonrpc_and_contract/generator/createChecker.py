import os
import hashlib
N = int(os.environ['N'])
COPIES = int(os.environ['COPIES'])

file = open("../checker/checker.py", "w")

file.write("""
import hashlib
import random

dumps = [
""")

for i in range(1, N + 1):
    file.write("[\n")
    for j in range(COPIES):
        file.write("\"" + open('./dumps%d/%d/input' % (j, i), "r").read() + ("\"\n" if j == COPIES - 1 else "\",\n"))
    file.write("]\n]\n" if i == N else "],\n")


file.write("""
answers = {
""")

for i in range(1, N + 1):
    for j in range(COPIES):
        hash = hashlib.sha256(open('./dumps%d/%d/input' % (j, i), "rb").read()).hexdigest()
        clue = open('./dumps%d/%d/clue' % (j, i), "r").read()
        file.write("\"%s\": \"%s\", \n" % (hash, clue[:-1]) if (i != N or j != COPIES - 1) else "\"%s\": \"%s\" \n" % (hash, clue[:-1]))


file.write("""
}

def generate():
    test = ''
    for i in range(13):
        test += random.choice(dumps[i]) + '\\n'
    return test


def solve(dataset):
    clue = ''
    for case in dataset.split('\\n')[:-1]:
        clue += answers[hashlib.sha256(case.encode()).hexdigest()] + '\\n'
    return clue


def check(reply, clue):
    return reply.strip() == clue.strip()

a = generate()

tests = [
    (a, solve(a), solve(a))
]
""")