## Tests generation

Each test by default consists of at most 4 clients, and 1 JSON-RPC node. 
During each test, each client executes its own instructions, described in
`./generator/scenarios/$TEST/s$N.json`.

In order to generate new test set, do the following:
1. Modify scenarios in `./generator/scenarios` for each test for each of the clients. Create 
more folders with tests if needed. The correct client, which is required as an answer, 
should always be the first one. 
2. Run tests generator (docker, docker-compose, node, python3 should be installed):
    ```shell script
    $ cd ./generator
    $ N=13 TEST_SET=0 ./generate.sh
    ```
    This will run a separate ganache-cli and a set of clients for each test. All local 
    ips will be replaced by random ones. All caught 
    traffic between JSON-RPC and clients, will be stored in `./generator/dumps$TEST_SET/$TEST/dump.pcap`.
    This is a usual pcap file. Short version of pcap file (omitting global and packet headers)
    will be saved in `./generator/dumps$TEST_SET/$TEST/input` in hex format. Expected answer will 
    be stored in `./generator/dumps$TEST_SET/$TEST/clue`. Received solution answer will be saved in
    `./generator/dumps$TEST_SET/$TEST/solution`.
3. Quick check that reference solution answers and expected ones are the same by running:
    ```shell script
    $ cd ./generator
    $ N=13 TEST_SET=0 ./check.sh
    ``` 
4. Create tests in stepik format by running:
    ```shell script
    $ cd ./generator
    $ N=13 TEST_SET=0 ./createTests.sh
    ```
5. Generate checker script for stepik Data step:
    ```shell script
    $ cd ./generator
    $ N=13 COPIES=5 python ./createChecker.py
    ```