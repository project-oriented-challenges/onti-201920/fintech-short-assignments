const express = require('express')
const {spawn} = require('child_process')

const app = express()

let childProcess
let startRequests = []
let stopRequests = []

app.get('/start', async function (req, res) {
    console.log('Adding start vote')
    startRequests.push(res)
    if (startRequests.length === 4) {
        console.log('Starting writing snapshot')
        childProcess = spawn('tcpdump', ['-w', './dumps/dump.pcap', '-i', 'eth0', '(tcp or udp) and port 8545'])
        childProcess.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });

        childProcess.stderr.on('data', (data) => {
            console.log(`stderr: ${data}`);
        });

        await new Promise(r => setTimeout(r, 10000))
        startRequests.forEach(r => r.send(true))
    }
})

app.get('/stop', async function (req, res) {
    console.log('Adding stop vote')
    stopRequests.push(res)
    if (stopRequests.length === 4) {
        await new Promise(r => setTimeout(r, 10000))
        console.log('Stopping writing snapshot')
        childProcess.kill('SIGKILL')
        childProcess = null
        stopRequests.forEach(r => r.send(true))
        process.exit()
    }
})

app.listen(6000)