set -e

for (( i = 1; i <= $N; i+=1 )); do
  fi=$(printf "%03d\n" $i)
  cp "./dumps/$i/clue" "../tests/$fi.clue"
  cp "./dumps/$i/input" "../tests/$fi"
done