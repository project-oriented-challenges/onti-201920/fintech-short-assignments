for (( i = 1; i <= $N; i+=1 )); do
  var=$(diff "./dumps$TEST_SET/$i/clue" "./dumps$TEST_SET/$i/solution")
  if [[ $var ]]; then
 		echo -e "Test $i \033[0;31mFAILED\033[0m:"
 		echo $var
 	else
 		echo -e "Test $i \033[0;32mPASSED\033[0m:"
 	fi
done