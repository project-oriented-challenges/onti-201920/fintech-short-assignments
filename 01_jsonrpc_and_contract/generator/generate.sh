set -e

for (( i = 1; i <= $N; i+=1 )); do
  mkdir "./dumps$TEST_SET/$i" > /dev/null 2>&1 || true
  echo "-" > "./dumps$TEST_SET/$i/solution"

  echo "Generating dump for test $i"
  TEST=$i docker-compose up --build

  echo "Replacing ip addresses for random ones"
  TEST=$i node ./replaceIp.js

  echo "Running solution"
  TEST=$i cat "./dumps$TEST_SET/$i/input" | ONLY_ONE=true python3 "../solution/solution.py" > "./dumps$TEST_SET/$i/solution"
  cat "./dumps$TEST_SET/$i/solution"

  echo "Running diff"
  diff "./dumps$TEST_SET/$i/clue" "./dumps$TEST_SET/$i/solution" || true
done