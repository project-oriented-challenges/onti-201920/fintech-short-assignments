pragma solidity ^0.5.11;

contract ONTI201920 {
    address creator;
    bool allowance = false;
    bytes32 public salted;

    constructor() public {
        creator = msg.sender;
    }

    modifier onlyCreator() {
        require(msg.sender == creator);
        _;
    }

    function callMe(bytes32 _salt) internal view returns(bytes32){
        return keccak256(abi.encodePacked(creator, _salt));
    }

    function invokeMe(bool _flag) onlyCreator public {
        allowance = _flag;
    }

    function setSalted() public {
        require(msg.sender == creator || allowance == true);
        salted = callMe(salted);
    }
}