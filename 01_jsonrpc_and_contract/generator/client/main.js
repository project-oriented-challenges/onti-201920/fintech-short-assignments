const Web3 = require('web3')
const RLP = require('rlp')
const axios = require('axios')
const os = require('os')
const fs = require('fs')
const {execSync} = require('child_process')

const privateKeys = require('./privateKeys')
const scenario = require('./scenario')

const web3 = new Web3('http://server:8545')

console.log('My ip is', os.networkInterfaces().eth0[0].address)

async function sendTx(tx) {
    await axios.post('http://server:8545',
        {
            jsonrpc: "2.0",
            id: 2,
            method: "eth_sendRawTransaction",
            params: [tx]
        },
        {
            headers: {
                'content-type': 'application/json',
                'connection': 'keep-alive',
                'Host': 'server:8545',
                'user-agent': 'Mozilla/5.0 (Linux x64) node.js/12.10.0 v8/7.6.303.29-node.16',
                'content-length': '354',
                'cookie2': ' ',
                'cookie': ' '
            }
        }
    )
}

if (fs.existsSync('./answer'))
    fs.writeFileSync('./answer/clue', Buffer.from(os.networkInterfaces().eth0[0].address, 'utf-8'))

const bytecode = '0x60806040526000805460ff60a01b1916905534801561001d57600080fd5b50600080546001600160a01b031916331790556101808061003f6000396000f3fe608060405234801561001057600080fd5b50600436106100415760003560e01c80631f9815161461004657806323a48c6c14610060578063e42ab25d1461006a575b600080fd5b61004e610089565b60408051918252519081900360200190f35b61006861008f565b005b6100686004803603602081101561008057600080fd5b503515156100cf565b60015481565b6000546001600160a01b03163314806100b65750600054600160a01b900460ff1615156001145b6100bf57600080fd5b6100ca600154610104565b600155565b6000546001600160a01b031633146100e657600080fd5b60008054911515600160a01b0260ff60a01b19909216919091179055565b6000546040805160609290921b6bffffffffffffffffffffffff1916602080840191909152603480840194909452815180840390940184526054909201905281519101209056fea265627a7a7231582072d85b2b318e3743651d18a702c04ff0ecb99ef441a76f3801888d826a86625764736f6c634300050b0032'
const abi = [
    {
        "constant": true,
        "inputs": [],
        "name": "salted",
        "outputs": [
            {
                "internalType": "bytes32",
                "name": "",
                "type": "bytes32"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [],
        "name": "setSalted",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "internalType": "uint256",
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "transfer",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": false,
        "inputs": [
            {
                "internalType": "bool",
                "name": "_flag",
                "type": "bool"
            }
        ],
        "name": "invokeMe",
        "outputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "constant": true,
        "inputs": [
            {
                "internalType": "bytes32",
                "name": "_salt",
                "type": "bytes32"
            }
        ],
        "name": "callMe",
        "outputs": [
            {
                "internalType": "bytes32",
                "name": "",
                "type": "bytes32"
            }
        ],
        "payable": false,
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [],
        "payable": false,
        "stateMutability": "nonpayable",
        "type": "constructor"
    }
]


let contract
let nonce
let privateKey
let address

async function main() {
    await new Promise(r => setTimeout(r, 5000))
    for (let command of scenario) {
        console.log(command)
        let [a, b, c] = command.split(' ')
        switch (a) {
            case 'account':
                privateKey = privateKeys[parseInt(b)]
                address = getAddr(parseInt(b))
                nonce = 0
                break
            case 'nonce':
                nonce = parseInt(b)
                break
            case 'contract':
                const addr = '0x' + web3.utils.sha3(RLP.encode([getAddr(parseInt(b)), parseInt(c)])).substr(26)
                contract = new web3.eth.Contract(abi, addr)
                break
            case 'deploy':
                await deploy()
                break
            case 'sleep':
                await new Promise(r => setTimeout(r, parseInt(b)))
                break
            case 'salted':
                await contract.methods.salted().call()
                break
            case 'invokeMe':
                await invokeMe(b)
                break
            case 'setSalted':
                await setSalted(b)
                break
            case 'callMe':
                try {
                    await contract.methods.callMe('0x0000000000000000000000000000000000000000000000000000000000000000').call()
                } catch (e) {

                }
                break
            case 'transfer':
                await transfer()
                break
            case 'start':
                await axios.get('http://server:6000/start')
                break
            case 'stop':
                await axios.get('http://server:6000/stop')
                break
            case 'udp':
                //const ip = execSync('ping -c 1 server | grep from | awk {\'print $4\'}').toString()
                execSync('echo some_random_message | nc -u -w0 server 8545')
                //execSync(`echo some_random_message > /dev/udp/${ip}/8545`)
                break
            default:
                console.error('Wrong command')
                break
        }
    }
}

function getAddr(n) {
    return web3.eth.accounts.privateKeyToAccount(privateKeys[n]).address
}

async function deploy() {
    const tx = {
        data: bytecode,
        from: address,
        nonce: nonce++,
        chainId: 1337,
        gas: 283410
    }
    const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey)
    await sendTx(signedTx.rawTransaction)
    contract = new web3.eth.Contract(abi, '0x' + web3.utils.sha3(RLP.encode([address, nonce - 1])).substr(26))
}

async function invokeMe(error) {
    const tx = {
        data: contract.methods.invokeMe(error !== 'false').encodeABI(),
        from: address,
        to: contract.options.address,
        nonce: nonce++,
        chainId: 1337,
        gas: error === 'false' ? 28000 : 31000
    }
    if (error === 'desc')
        tx.data = tx.data.substr(0, 10)
    else if (error === 'pay')
        tx.value = '1'
    else if (error === 'gas')
        tx.gas = 21200
    const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey)
    try {
        await sendTx(signedTx.rawTransaction)
    } catch (e) {
    }
}

async function setSalted(error) {
    const tx = {
        data: contract.methods.setSalted().encodeABI(),
        from: address,
        to: contract.options.address,
        nonce: nonce++,
        chainId: 1337,
        gas: 45000
    }
    if (error === 'desc')
        tx.data = tx.data.substr(0, 10)
    else if (error === 'pay')
        tx.value = '1'
    else if (error === 'gas')
        tx.gas = 22000
    const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey)
    try {
        await sendTx(signedTx.rawTransaction)
    } catch (e) {
    }

}

async function transfer() {
    const tx = {
        data: contract.methods.transfer('0xf43f40a1e7b152f6304ac83ae04912071f4be3b3', 41265).encodeABI(),
        from: address,
        to: contract.options.address,
        nonce: nonce++,
        chainId: 1337,
        gas: 80000
    }
    const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey)
    try {
        await sendTx(signedTx.rawTransaction)
    } catch (e) {
    }
}

main()