const fs = require('fs')
let data = fs.readFileSync(`./dumps${process.env.TEST_SET}/${process.env.TEST}/dump.pcap`)
let correctAnswer = fs.readFileSync(`./dumps${process.env.TEST_SET}/${process.env.TEST}/clue`).toString()

let offset = 24

const buffers = []

while (offset < data.length) {
    offset += 16

    const len = data[offset + 16] * 256 + data[offset + 17] + 14
    buffers.push(data.slice(offset, offset + len))

    offset += len
}

data = Buffer.concat(buffers)
//fs.writeFileSync(`./dumps/${process.env.TEST}/dump.min.pcap`, data)

offset = 0

const ips = {}

function getIp(ip) {
    return ips[ip] ? ips[ip] : ips[ip] = Math.floor(Math.random() * 256 ** 4)
}

while (offset < data.length) {
    const len = data[offset + 16] * 256 + data[offset + 17] + 14
    const srcIp = data.readUInt32BE(offset + 26)
    const destIp = data.readUInt32BE(offset + 30)

    //console.log(data.slice(offset, offset + len), len, srcIp, destIp)
    data.writeUInt32BE(getIp(srcIp), offset + 26)
    data.writeUInt32BE(getIp(destIp), offset + 30)

    offset += len
}

function formatIp(ip) {
    const buf = Buffer.allocUnsafe(4)
    buf.writeUInt32BE(ip)
    return `${buf[0]}.${buf[1]}.${buf[2]}.${buf[3]}`
}

Object.entries(ips).forEach(([x,y]) => {
    if (formatIp(x) === correctAnswer)
        fs.writeFileSync(`./dumps${process.env.TEST_SET}/${process.env.TEST}/clue`, Buffer.from(formatIp(y) + '\n'))
    console.log(formatIp(x), '->', formatIp(y))
})

fs.writeFileSync(`./dumps${process.env.TEST_SET}/${process.env.TEST}/input`, data.toString('hex'))
