import hashlib


def get_hex_code(chat_id: int) -> str:
    chat_id_bytes = str(abs(chat_id)).encode()
    return hashlib.sha256(chat_id_bytes).hexdigest()
