from aiogram import Bot, Dispatcher, executor
from bot.add_to_group_middleware import AddToGroupMiddleware
import asyncio
import os
import logging
logging.basicConfig(level=logging.INFO, filename="/bot/log")


BOT_TOKEN = os.getenv('BOT_TOKEN')
loop = asyncio.get_event_loop()
bot = Bot(BOT_TOKEN, loop=loop)
dp = Dispatcher(bot)

if __name__ == '__main__':
    dp.middleware.setup(AddToGroupMiddleware())
    executor.start_polling(dp)
