import sys
from pprint import pprint
from bot1.bot.util import get_hex_code as get_hex_code_1
from bot2.bot.util import get_hex_code as get_hex_code_2
from bot3.bot.util import get_hex_code as get_hex_code_3
from bot4.bot.util import get_hex_code as get_hex_code_4


def get_hash(chat_id: int, image_number: int):
    if image_number == 1:
        return get_hex_code_1(chat_id)
    elif image_number == 2:
        return get_hex_code_2(chat_id)
    elif image_number == 3:
        return get_hex_code_3(chat_id)
    elif image_number == 4:
        return get_hex_code_4(chat_id)


images = sys.argv[1:5]
chat_ids = list(map(int, sys.argv[5::2]))
chat_links = sys.argv[6::2]

tests = []
for i in range(len(images)):
    image = images[i]

    for j in range(len(chat_ids)):
        chat_id = chat_ids[j]
        chat_link = chat_links[j]

        answer = get_hash(chat_id, i+1)
        tests.append((f'image: {image}\ngroup: {chat_link}', answer))

pprint(tests)
