from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from bot.util import get_hex_code
import logging


class AddToGroupMiddleware(BaseMiddleware):
    @staticmethod
    async def on_pre_process_update(update: types.Update, data: dict):
        bot_id = (await update.bot.get_me()).id
        new_member_ids = [member.id for member in update.message.new_chat_members]

        if bot_id in new_member_ids:
            hex_code = get_hex_code(update.message.chat.id)
            logging.info(f'Bot was added to group, hex number of connection is {hex_code}')
