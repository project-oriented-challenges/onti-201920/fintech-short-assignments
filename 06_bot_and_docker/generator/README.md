Генератор тестовых данных
====

Данная директория содержит генератор тестовых данных задачи.

Под генератором подразумевается:
  * исходный код подсистем, использующихся для генерации данных
  * скрипты автоматизирующие генерацию данных
  * инструкция, как запустить генератор данных и т.п.

Цель - предоставить возможность повтороно перегенерировать данные, если такая необходимость возникнет в будущем.

Генерация данных для задания №6:
1) Собрать и загрузить docker-образы ботов (```docker build & docker push```), получить 4 ссылки на образы
1) Создать 4 группы в Telegram
2) Узнать chatId групп. Для этого нужно добавить бота @RawDataBot в группу, и скопировать поле message.chat.id. После удалить @RawDataBot из группы
3) Узнать ссылки на группы
4) Запустить скрипт generate.py с аргументами: 4 ссылки на образы ботов, 4 пары [chatId chatLink]

Пример: 

```python3 generate.py vadimkerr/onti2019-problem6:1 vadimkerr/onti2019-problem6:2 vadimkerr/onti2019-problem6:3 vadimkerr/onti2019-problem6:4 -358570102 https://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw -319777851 https://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA -359319456 https://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ -274366454 https://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA```

Вывод скрипта: 

```
[('image: vadimkerr/onti19-problem6:1\n'
  'group: https://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw',
  '0efc5bbf85d21ad2db735f9e039d58b4baa3952385dc7e1b926eb628190bd463'),
 ('image: vadimkerr/onti19-problem6:1\n'
  'group: https://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA',
  '02b964bede71bde4a2e60005b383fcba56e83526ab577e877c7357899ae74b8c'),
 ('image: vadimkerr/onti19-problem6:1\n'
  'group: https://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ',
  '6b6b47dd7678df418f30749d91f3d8e7d2da6c9e05c6bcb1c2abdc6f04cbceb4'),
 ('image: vadimkerr/onti19-problem6:1\n'
  'group: https://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA',
  'd008d2d836b9ce4735820c2222a063434cb66b9238531ec5afa7ca7056ce2532'),
 ('image: vadimkerr/onti19-problem6:2\n'
  'group: https://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw',
  '31cd629bee45eb00bf0c47b71cd536bb716a0d96c09099c3536eb08b600c00ef'),
 ('image: vadimkerr/onti19-problem6:2\n'
  'group: https://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA',
  'a81cb1a1250cdbb84a1ad0cda1b035b6e5f031510875b606d270cef78023b864'),
 ('image: vadimkerr/onti19-problem6:2\n'
  'group: https://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ',
  '7eaea2e0920d817a10b7898dfdc52a201e21a843df270a2c1a4644b9357537da'),
 ('image: vadimkerr/onti19-problem6:2\n'
  'group: https://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA',
  '8ccd7c83bca285b14c1f0ed5a756fd790e58d15f941703a97c502b19a76b3dcf'),
 ('image: vadimkerr/onti19-problem6:3\n'
  'group: https://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw',
  '1a878cb929a1f71d41f8619687f4bfc38b5fcb43a96569203f1e286349909a2c'),
 ('image: vadimkerr/onti19-problem6:3\n'
  'group: https://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA',
  '5084bd98589b81ec2bab8cd161f2f49707952c5a55b567ebce3c54068d0ac061'),
 ('image: vadimkerr/onti19-problem6:3\n'
  'group: https://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ',
  '0286027a27339cb8f3ca2d51c276ba29c56b60771c76713dfdfa74f6cffab3cb'),
 ('image: vadimkerr/onti19-problem6:3\n'
  'group: https://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA',
  '17a747baa8570aff0fa07f6c9297528061d5a11cd04e55dcb55783f70cf82339'),
 ('image: vadimkerr/onti19-problem6:4\n'
  'group: https://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw',
  '47f65261f327825b7cca2f1956de3b722184bb39f2f8901dbfd8c072f15086ce'),
 ('image: vadimkerr/onti19-problem6:4\n'
  'group: https://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA',
  'e8053211561efd38fb99e177cdc34a05bfb7d1dd55b51c5244d1b8614c37c199'),
 ('image: vadimkerr/onti19-problem6:4\n'
  'group: https://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ',
  '24d2f600d9bb67560ecf6323a27b068b2cd325c316b90ff036f92e2578212ebb'),
 ('image: vadimkerr/onti19-problem6:4\n'
  'group: https://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA',
  '32595ae3d3772e93d2a9c1ede3c2fe69fbab350140aaa0eb8550fe373f4a0fc8')]
  ```
