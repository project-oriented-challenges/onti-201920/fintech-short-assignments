import hashlib


def get_hex_code(chat_id: int) -> str:
    chat_id_bytes = str(chat_id)[3:].encode()
    return hashlib.sha256(chat_id_bytes).hexdigest()
