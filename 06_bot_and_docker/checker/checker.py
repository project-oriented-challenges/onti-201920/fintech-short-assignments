from random import choice

data = {
    'akolotov/botforchat:test\nhttps://t.me/joinchat/AklWixb-YE-4iv6a_X-wgw\n': '87d3651ebc7c7d3ab93cb3649403ff19c174ee427ade2b679190ac5af9e76568',
    'vadimkerr/onti2019-problem6:1\nhttps://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw\n': '0efc5bbf85d21ad2db735f9e039d58b4baa3952385dc7e1b926eb628190bd463',
    'vadimkerr/onti2019-problem6:1\nhttps://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA\n': '02b964bede71bde4a2e60005b383fcba56e83526ab577e877c7357899ae74b8c',
    'vadimkerr/onti2019-problem6:1\nhttps://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ\n': '6b6b47dd7678df418f30749d91f3d8e7d2da6c9e05c6bcb1c2abdc6f04cbceb4',
    'vadimkerr/onti2019-problem6:1\nhttps://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA\n': 'd008d2d836b9ce4735820c2222a063434cb66b9238531ec5afa7ca7056ce2532',
    'vadimkerr/onti2019-problem6:2\nhttps://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw\n': '31cd629bee45eb00bf0c47b71cd536bb716a0d96c09099c3536eb08b600c00ef',
    'vadimkerr/onti2019-problem6:2\nhttps://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA\n': 'a81cb1a1250cdbb84a1ad0cda1b035b6e5f031510875b606d270cef78023b864',
    'vadimkerr/onti2019-problem6:2\nhttps://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ\n': '7eaea2e0920d817a10b7898dfdc52a201e21a843df270a2c1a4644b9357537da',
    'vadimkerr/onti2019-problem6:2\nhttps://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA\n': '8ccd7c83bca285b14c1f0ed5a756fd790e58d15f941703a97c502b19a76b3dcf',
    'vadimkerr/onti2019-problem6:3\nhttps://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw\n': '1a878cb929a1f71d41f8619687f4bfc38b5fcb43a96569203f1e286349909a2c',
    'vadimkerr/onti2019-problem6:3\nhttps://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA\n': '5084bd98589b81ec2bab8cd161f2f49707952c5a55b567ebce3c54068d0ac061',
    'vadimkerr/onti2019-problem6:3\nhttps://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ\n': '0286027a27339cb8f3ca2d51c276ba29c56b60771c76713dfdfa74f6cffab3cb',
    'vadimkerr/onti2019-problem6:3\nhttps://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA\n': '17a747baa8570aff0fa07f6c9297528061d5a11cd04e55dcb55783f70cf82339',
    'vadimkerr/onti2019-problem6:4\nhttps://t.me/joinchat/NbaWwBVfWHaif8cxhGeufw\n': '47f65261f327825b7cca2f1956de3b722184bb39f2f8901dbfd8c072f15086ce',
    'vadimkerr/onti2019-problem6:4\nhttps://t.me/joinchat/NbaWwBMPbDuKoM4pKM50BA\n': 'e8053211561efd38fb99e177cdc34a05bfb7d1dd55b51c5244d1b8614c37c199',
    'vadimkerr/onti2019-problem6:4\nhttps://t.me/joinchat/NbaWwBVqx6DTD0zdofx6FQ\n': '24d2f600d9bb67560ecf6323a27b068b2cd325c316b90ff036f92e2578212ebb',
    'vadimkerr/onti2019-problem6:4\nhttps://t.me/joinchat/NbaWwBBaf_at5HTs5Z-LRA\n': '32595ae3d3772e93d2a9c1ede3c2fe69fbab350140aaa0eb8550fe373f4a0fc8'
}

def generate():
    return choice(list(data.keys()))

def solve(dataset):
    return data[dataset]

def check(reply, clue):
    return reply.strip() == clue.strip()

tests = [
    ("akolotov/botforchat:test\nhttps://t.me/joinchat/AklWixb-YE-4iv6a_X-wgw\n",
     "87d3651ebc7c7d3ab93cb3649403ff19c174ee427ade2b679190ac5af9e76568",
     "87d3651ebc7c7d3ab93cb3649403ff19c174ee427ade2b679190ac5af9e76568")
]
