import re
import copy
import sys

# keywords

FUNCTION_DEFINITION = "func"
RETURN = "return"
ASSIGN_VARIABLE = "setq"
BODY = "prog"
WHILE = "while"
BREAK = "break"
READ_VALUE = "read"
BRANCH = "cond"

# my keywords
BEGIN = '__begin__'
EMPTY = '_empty'
HEAD = 'head'


class Tree:
  def __init__(self, node=None):
    if node is None:
      node = Node('head')
    self.__head = node
    self.__current = self.__head

  def prev(self):
    if not self.__current.parent is None:
      self.__current = self.__current.parent

  def get_current(self):
    return self.__current

  def add_and_go(self, name):
    child = Node(name, parent=self.__current)
    self.__current.add_child(child)
    self.__current = child

  def add(self, name):
    child = Node(name, parent=self.__current)
    self.__current.add_child(child)

  def setHead(self):
    self.__current = self.__head

  def __str__(self):
    return str(self.__head)

  def __repr__(self):
    return str(self)


class Node:
  def __init__(self, name, parent=None, children=None):
    self.name = name
    self.parent = parent
    if not children is None:
      if isinstance(children, list):
        self.children = children
      else:
        self.children = [children]
    else:
      self.children = []

  def add_child(self, child):
    if isinstance(child, list):
      self.children.extend(child)
    else:
      self.children.extend([child])

  def __str__(self):

    return f"{self.name}{': ' + str(self.children) if len(self.children) > 0 else ''}"

  def __repr__(self):
    return str(self)


# -----------------------------------
# Functions that help generate a tree
# -----------------------------------


def make_code_easy(tokenized_code: list):
  # makes more easy to build a AST
  # ( prog ( ( setq x 1 ) ( return x ) ) ) -> ( prog ( __begin__ ( setq x 1 ) ( return 1 ) ) )
  #
  # ( func a ( ) 1 )                       -> ( func a ( _empty ) 1 )
  for i in range(len(tokenized_code)):
    if tokenized_code[i] == '(':
      if tokenized_code[i + 1] == '(':
        tokenized_code.insert(i + 1, '__begin__')

    if tokenized_code[i] == '(':
      if tokenized_code[i + 1] == ')':
        tokenized_code.insert(i + 1, '_empty')

  return tokenized_code


def tokenizeCode(code):

  code = code.split()
  return code


def buildSyntaxTree(code):
  tokens = make_code_easy(tokenizeCode(code))
  tree = Tree()
  for i in range(1, len(tokens)):

    if tokens[i] == '(':
      pass
    elif tokens[i] == ')':
      tree.prev()
    elif tokens[i - 1] == '(':
      tree.add_and_go(tokens[i])
    else:
      tree.add(tokens[i])
  return tree


# ---------------------------------
# Functions that generate bytecode
# ---------------------------------


def putValueOnStack(value, size=None):
  assert value >= 0, "We cannot handle with negative numbers"
  value = hex(value)[2:]
  if size is None:
    push_command_lenght = (len(value) + 1) // 2
  else:
    push_command_lenght = size

  if push_command_lenght > 32:
    raise Exception(f"too big number: {value}")
  return f"{hex(0x60 + push_command_lenght - 1)[2:]}{value.zfill(push_command_lenght * 2)}"


def putValueOfMemoryOnStack(index):
  return putValueOnStack(index) + "51"


def putValueInMemory(index, code_to_put_value_on_stack):
  code_to_put_address_on_stack = putValueOnStack(index)
  return code_to_put_value_on_stack + code_to_put_address_on_stack + '52'


def dup(amount):
  if amount > 0:
    return f"{hex(0x80 + amount - 1)[2:]}"
  if amount == 0:
    return ''
  raise (Exception('Number for dup has to be greater or equal to 0'))


def swap(amount):
  if amount > 0:
    return f"{hex(0x90 + amount - 1)[2:]}"
  if amount == 0:
    return ''
  raise (Exception('Number for swap has to be greater or equal to 0'))


def return_value_from_stack_by_index(index):
  return swap(index) + "60005260206000f3"


# -----------------------------
# Functions that support logic
# -----------------------------


def check_amount_of_args(tree: Tree, required_value):
  tree.setHead()
  function_name = tree.get_current().name
  if isinstance(required_value, int):
    required_value = [required_value]

  if tree.get_current().children.__len__() not in required_value:
    raise Exception(
        f"Function/keyword {function_name} needs {required_value} args. \n Tree: {tree}"
    )


def is_variable(name: str):
  return name[0].isalpha()


def change_all_values_to_delta(dct, delta):
  for value in dct:
    dct[value] += delta


def find_free_space(variables):
  if variables:
    return variables[max(variables, key=lambda x: variables[x])] + 0x20
  return 0x00


def insert_into_string(string, index, end, value):
  return string[:index] + str(value) + string[end:]


def remove_while_from_string(string):
  while not re.search('\|\|[0-9, a-z, A-Z, -, _]+\|\|', string) is None:
    match = re.search('\|\|[0-9, a-z, A-Z, -, _]+\|\|', string)
    index, end = match.start(), match.end()
    string = insert_into_string(string, index, end, '')
  return string


def get_real_len_in_bytes(string):
  string = remove_while_from_string(string)
  length = len(string)
  while not re.search('__[0-9, a-z, A-Z, -]+__', string) is None:
    match = re.search('__[0-9, a-z, A-Z, -]+__', string)
    index, end = match.start(), match.end()
    length -= end - index
    length += 32
    string = insert_into_string(string, index, end, '')
  return length // 2


def replace_dev_strings(string: str, string_with_while: str, function_indexes):
  res = string
  while not re.search('__[0-9, a-z, A-Z, -]+__', res) is None:
    match = re.search('__[0-9, a-z, A-Z, -]+__', res)
    index, end = match.start(), match.end()

    value_to_put = ''
    if res[index + 2:index + 4] == '-f':
      name = res[index + 4:end - 2]
      value_to_put = function_indexes[name][0]
      value_to_put = putValueOnStack(value_to_put, size=15)
    elif res[index + 2:index + 4] == 'sh':
      value_to_put = int(res[index + 7:end - 2])
      value_to_put = putValueOnStack(value_to_put + index // 2 + 16, size=15)
    elif res[index + 2:index + 4] == 'br':
      match = re.search('__break__', string_with_while)
      index2, end2 = match.start(), match.end()
      temp = string_with_while[:index2]
      for match in re.finditer('\|\|[0-9, a-z, A-Z, -, _]+\|\|', temp):
        index3, end3 = match.start(), match.end()

      index4 = get_real_len_in_bytes(temp[:index3])
      value_to_put = int(temp[index3 + 7:end3 - 2])
      value_to_put = putValueOnStack(value_to_put + index4, size=15)
      string_with_while = insert_into_string(string_with_while, index3, end3,
                                             '')
      string_with_while = insert_into_string(string_with_while,
                                             index2 - end3 + index3,
                                             end2 - end3 + index3,
                                             "__nothing__")

    res = insert_into_string(res, index, end, value_to_put)
  return res


# ---------------------------------
# Functions that represent keywords
# ---------------------------------


def __function_definition(tree: Tree, variables, f_variables,
                          function_indexes):
  check_amount_of_args(tree, [3])  # f_name, args, body
  func_name = tree.get_current().children[0].name
  args = [tree.get_current().children[1].name
          ] + [i.name for i in tree.get_current().children[1].children]

  # так называемый КОСТЫЛЬ
  if args == ['_empty']:
    args = []
  args = {arg: i for i, arg in enumerate(args)}
  f_variables[func_name] = args
  function_indexes[func_name][2] = len(args)
  code_of_function, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[2]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)

  # now, we have to find jump dest to leave the function
  # stack after code_of_function executing:
  #       [ result, arg, arg, ... , arg, jump_dest ]
  #            0     1    2          n     n + 1
  if len(f_variables[func_name]) > 0:
    k = f_variables[func_name][max(f_variables[func_name],
                                   key=lambda x: f_variables[func_name][x])]
  else:
    k = 0
  return code_of_function + swap(k) + '50' * k + swap(
      1) + '56', variables, f_variables, function_indexes


def __return(tree: Tree,
             variables,
             f_variables,
             function_indexes,
             func_name=None):
  check_amount_of_args(tree, [1])
  value, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[0]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)
  if not func_name is None:
    # res trash arg arg arg arg dest
    # 0                      k
    if len(f_variables[func_name]) > 0:
      k = f_variables[func_name][max(f_variables[func_name],
                                     key=lambda x: f_variables[func_name][x])]
    else:
      k = 0
    return value + swap(k) + '50' * k + swap(
        1) + '56', variables, f_variables, function_indexes

  return value + return_value_from_stack_by_index(
      0), variables, f_variables, function_indexes


def __assign_variable(tree: Tree,
                      variables,
                      f_variables,
                      function_indexes,
                      func_name=None):
  check_amount_of_args(tree, [2])

  var_name = tree.get_current().children[0].name

  # If we in function
  if not func_name is None:
    # [ some_value, ... , arg, arg, ...]
    #       0              k
    # all values from 0 to k go to memory, add new variable and add all values from memory:
    #
    # [ some_value, ... , NEW_VALUE, arg, arg, ...]
    #
    # k = 3, insert x = 5
    # [ 1, 4, 11, arg, arg] => [ 1, 4, 11, 5, arg, arg]

    # If variable doesnt exist
    if var_name not in f_variables[func_name]:
      if len(f_variables[func_name]) > 0:
        k = f_variables[func_name][min(
            f_variables[func_name], key=lambda x: f_variables[func_name][x])]
      else:
        k = 0
      start_index = find_free_space(variables)
      put_all_values_in_memory = ''

      for index in range(0, 32 * k, 32):
        put_all_values_in_memory += f"{putValueOnStack(index + start_index)}52"

      return_all_values_on_stack = ''
      for index in reversed(range(0, 32 * k, 32)):
        return_all_values_on_stack += f"{putValueOnStack(index + start_index)}51"

      change_all_values_to_delta(f_variables[func_name], -k)
      value, variables, f_variables, function_indexes = evaluate(
          Tree(tree.get_current().children[1]),
          variables,
          f_variables,
          function_indexes,
          func_name=func_name)
      change_all_values_to_delta(f_variables[func_name], k)

      f_variables[func_name][var_name] = k

      return put_all_values_in_memory + value + return_all_values_on_stack, variables, f_variables, function_indexes

    else:
      k = f_variables[func_name][var_name]
      value, variables, f_variables, function_indexes = evaluate(
          Tree(tree.get_current().children[1]),
          variables,
          f_variables,
          function_indexes,
          func_name=func_name)
      change_all_values_to_delta(f_variables[func_name], -1)
      return value + swap(k +
                          1) + '50', variables, f_variables, function_indexes

  # If we not in function
  else:
    value, variables, f_variables, function_indexes = evaluate(
        Tree(tree.get_current().children[1]),
        variables,
        f_variables,
        function_indexes,
        func_name=func_name)
    if var_name not in variables:
      index = find_free_space(variables)
      variables[var_name] = index
    else:
      index = variables[var_name]

    return putValueInMemory(index,
                            value), variables, f_variables, function_indexes


def __read(tree: Tree,
           variables,
           f_variables,
           function_indexes,
           func_name=None):
  check_amount_of_args(tree, [1])
  value, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[0]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)
  return value + '60200235', variables, f_variables, function_indexes  # value *= 32


def __begin(tree: Tree,
            variables,
            f_variables,
            function_indexes,
            func_name=None):
  result = ''
  for child in tree.get_current().children:
    res, variable, f_variables, function_indexes = evaluate(
        Tree(child),
        variables,
        f_variables,
        function_indexes,
        func_name=func_name)
    result += res
  return result, variables, f_variables, function_indexes


def __break(tree: Tree,
            variables,
            f_variables,
            function_indexes,
            func_name=None):
  check_amount_of_args(tree, [2])

  # idea:
  # [||while_address to END||] START [condition] 19600116 [JUMPI to END] [ACTION] [JUMP to START] END
  #  we need this for break                         NOT

  condition, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[0]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)
  condition += '19600116'
  if not func_name is None:
    change_all_values_to_delta(f_variables[func_name], -1)

  action, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[1]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)

  return f'||while{1 + get_real_len_in_bytes(condition) + 16 + get_real_len_in_bytes(action) + 16 + 2}||' + '5b' + \
         condition + f"__shift{get_real_len_in_bytes(action) + 2 + 16}__57" + action + \
         f"__shift{-get_real_len_in_bytes(action) - 16 - 16 - get_real_len_in_bytes(condition) - 2}__565b", \
         variables, f_variables, function_indexes


def __branch(tree: Tree,
             variables,
             f_variables,
             function_indexes,
             func_name=None):
  # cond (condition) (action1) (action2)
  # [bytecode for condition] 19600116 [jumpI to A] [action1] [jump to B] 5b [action2] 5b
  #                          (~a) & 1                                     A            B
  #
  # if action2 doesnt exist:
  # [bytecode for condition] 19600116 [jumpI to A] [action1] [jump to B] 5b 5b
  #                          (~a) & 1                                     A  B

  check_amount_of_args(tree, [2, 3])
  condition, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[0]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)
  condition += '19600116'

  if not func_name is None:
    change_all_values_to_delta(f_variables[func_name], -1)
  temp_variables, temp_f_variables = copy.deepcopy(variables), copy.deepcopy(
      f_variables)
  action1, variables, f_variables, function_indexes = evaluate(
      Tree(tree.get_current().children[1]),
      variables,
      f_variables,
      function_indexes,
      func_name=func_name)
  variables, f_variables = temp_variables, temp_f_variables

  action2 = '5b5b'
  if len(tree.get_current().children) > 2:
    temp_variables, temp_f_variables = copy.deepcopy(variables), copy.deepcopy(
        f_variables)
    action2, variables, f_variables, function_indexes = evaluate(
        Tree(tree.get_current().children[2]),
        variables,
        f_variables,
        function_indexes,
        func_name=func_name)
    variables, f_variables = temp_variables, temp_f_variables

    action2 = '5b' + action2 + '5b'
  # if not func_name is None:
  #    change_all_values_to_delta(f_variables[func_name], 1)

  return condition + f"__shift{get_real_len_in_bytes(action1) + 16 + 2}__57" + action1 + \
         f"__shift{get_real_len_in_bytes(action2)}__56" + action2, variables, f_variables, function_indexes


def __variable(tree: Tree,
               variables,
               f_variables,
               function_indexes,
               name,
               func_name=None):
  # If func_name is not None, than variable in function
  # and we have to support current position of each value of the function on a stack
  if not func_name is None:
    # Support current position of each value of function on stack
    change_all_values_to_delta(f_variables[func_name], 1)

    if name in f_variables[func_name]:
      return dup(f_variables[func_name]
                 [name]), variables, f_variables, function_indexes

  # otherwise variable in prog
  index = variables[name]
  return putValueOfMemoryOnStack(
      index), variables, f_variables, function_indexes


def __constant(tree: Tree,
               variables,
               f_variables,
               function_indexes,
               value,
               func_name=None):
  if not func_name is None:
    change_all_values_to_delta(f_variables[func_name], 1)
  return putValueOnStack(int(value)), variables, f_variables, function_indexes


def __function_call(tree: Tree,
                    variables,
                    f_variables,
                    function_indexes,
                    name,
                    func_name=None):
  # Default arithmetic functions:
  if name == "not":
    check_amount_of_args(tree, [1])
    value, variables, f_variables, function_indexes = evaluate(
        Tree(tree.get_current().children[0]),
        variables,
        f_variables,
        function_indexes,
        func_name=func_name)

    return value + "19600116", variables, f_variables, function_indexes

  command = {
      "plus": "01",
      "minus": "03",
      "times": "02",
      "divide": "04",
      "equal": "14",
      "less": "10",
      "greater": "11",
      "nonequal": "1419600116",
      "lesseq": "808214911117",
      "greatereq": "808214911017",
      "or": "17",
      "and": "16"
  }

  if name in command:
    check_amount_of_args(tree, [2])
    value2, variables, f_variables, function_indexes = evaluate(
        Tree(tree.get_current().children[1]),
        variables,
        f_variables,
        function_indexes,
        func_name=func_name)
    value1, variables, f_variables, function_indexes = evaluate(
        Tree(tree.get_current().children[0]),
        variables,
        f_variables,
        function_indexes,
        func_name=func_name)
    if not func_name is None:
      change_all_values_to_delta(f_variables[func_name], -1)

    return value2 + value1 + command[
        name], variables, f_variables, function_indexes

  # User functions
  else:
    if name not in function_indexes:
      raise Exception(
          f"Function {name} should be declared before the call.\nTree: {tree}")

    required_number_of_arguments = function_indexes[name][2]
    check_amount_of_args(tree, required_number_of_arguments)
    code_to_put_all_args_on_stack = ''

    # because of __shift__
    if not func_name is None:
      change_all_values_to_delta(f_variables[func_name], 1)

    for i in reversed(range(required_number_of_arguments)):
      res, variables, f_variables, function_indexes = evaluate(
          Tree(tree.get_current().children[i]),
          variables,
          f_variables,
          function_indexes,
          func_name=func_name)
      code_to_put_all_args_on_stack += res

    if not func_name is None:
      change_all_values_to_delta(f_variables[func_name],
                                 -required_number_of_arguments)

    jump = f'__-f{name}__56'
    return f"__shift{get_real_len_in_bytes(code_to_put_all_args_on_stack) + 1 + 16}__{code_to_put_all_args_on_stack}{jump}5b", \
           variables, f_variables, function_indexes


# ------------------------------------------
# Main function, that evaluate a syntax tree
# ------------------------------------------


def evaluate(tree: Tree,
             variables,
             f_variables,
             function_indexes,
             func_name=None):
  tree.setHead()
  name = tree.get_current().name

  # [+]
  if name == HEAD:
    result = ''
    body_of_function = {}
    for child in tree.get_current().children:
      child = Tree(child)
      if child.get_current().name == FUNCTION_DEFINITION:
        func_name = child.get_current().children[0].name

        # By now, we don't know the index of this function,
        # but we can already call it, during the execution (recursion)
        # and therefore we just put name of this function like this: __-fFUNCNAME__.
        # Later we will update it.
        function_indexes[func_name] = [0, 0, 0]
        res, variables, f_variables, function_indexes = evaluate(
            child,
            variables,
            f_variables,
            function_indexes,
            func_name=func_name)
        body_of_function[func_name] = '5b' + res

      else:
        assert len(variables) == 0

        # evaluate prog body. Now func_name is None
        result_of_prog, variables, f_variables, function_indexes = evaluate(
            Tree(child.get_current().children[0]),
            variables,
            f_variables,
            function_indexes,
            func_name=None)
        result_with_while = result_of_prog
        result = remove_while_from_string(result_of_prog)

        # we have to increase the start and the end value of all functions by the len of prog bytecode
        # + len of __shift__ , __-ffunc_name__ etc
        for func_name in function_indexes:
          body = body_of_function[func_name]
          index_start = get_real_len_in_bytes(result)
          index_end = index_start + get_real_len_in_bytes(body)

          result_with_while += body
          result += remove_while_from_string(body)

          function_indexes[func_name][0] = index_start
          function_indexes[func_name][1] = index_end

        result = replace_dev_strings(result, result_with_while,
                                     function_indexes)
    return result, variables, f_variables, function_indexes

  # [+]
  elif name == FUNCTION_DEFINITION:
    return __function_definition(tree, variables, f_variables,
                                 function_indexes)

  # [+]
  elif name == RETURN:
    return __return(tree,
                    variables,
                    f_variables,
                    function_indexes,
                    func_name=func_name)

  # [+]
  elif name == ASSIGN_VARIABLE:
    return __assign_variable(tree,
                             variables,
                             f_variables,
                             function_indexes,
                             func_name=func_name)

  # [+]
  elif name == READ_VALUE:
    return __read(tree,
                  variables,
                  f_variables,
                  function_indexes,
                  func_name=func_name)

  # [+]
  elif name == BEGIN:
    return __begin(tree,
                   variables,
                   f_variables,
                   function_indexes,
                   func_name=func_name)

  # [+]
  elif name == WHILE:
    return __break(tree,
                   variables,
                   f_variables,
                   function_indexes,
                   func_name=func_name)

  # [+]
  elif name == BREAK:
    return "__break__56", variables, f_variables, function_indexes

  # [+]
  elif name == BRANCH:
    return __branch(tree,
                    variables,
                    f_variables,
                    function_indexes,
                    func_name=func_name)

  # [+] User function call
  elif name in function_indexes:
    return __function_call(tree,
                           variables,
                           f_variables,
                           function_indexes,
                           name,
                           func_name=func_name)

  # [+] Variable or constant
  elif tree.get_current().children.__len__() == 0:

    # --- Variable ---
    if is_variable(name):
      return __variable(tree,
                        variables,
                        f_variables,
                        function_indexes,
                        name,
                        func_name=func_name)

    # --- Constant ---
    else:
      return __constant(tree,
                        variables,
                        f_variables,
                        function_indexes,
                        value=name,
                        func_name=func_name)

  # [+] Function call
  else:
    return __function_call(tree,
                           variables,
                           f_variables,
                           function_indexes,
                           name,
                           func_name=func_name)


if __name__ == "__main__":
  initial_code = sys.stdin.read()
  tree = buildSyntaxTree(initial_code)
  result, var, f_var, function_indexes = evaluate(tree, {}, {}, {},
                                                  func_name=None)
  print(result)
