dataset = dict()

# 1
# From the statement
dataset["""( prog ( 
  ( setq a ( read 0 ) )
  ( setq b ( read 1 ) )
  ( return ( minus a b ) ) )
)"""] = [[[26, 5], 21],
         [[2000000, 1000000], 1000000]]

# 2
# check `setq`
dataset["""( prog (
    ( setq x 1 )
    ( setq y 2 )
    ( setq y x )
    ( setq x 3 ) 
    ( return ( minus x y ) )
  )
)"""] = [[[], 2]]

# 3
# check simple `cond`
dataset["""( prog (
    ( setq n ( read 0 ) )
    ( setq v n )
    ( cond ( equal n 5 )
           ( setq v 25 )
    )
    ( return v )
) )"""] = [[[0], 0],
           [[5], 25],
           [[1000000000], 1000000000]]

# 4
# check full `cond` and ariphmetic operations
dataset["""( prog (
    ( setq n ( read 0 ) )
    ( cond ( equal n 0 )
           ( setq n ( times ( read 1 ) ( read 2 ) ) )
           ( cond ( equal n 1 ) 
                  ( setq n ( divide ( read 1 ) ( read 2 ) ) )
           )
    )
    ( return n )
) )"""] = [[[0, 2000, 5], 10000],
           [[1, 2000, 5], 400]]

# 5
# check stack of ariphmetic operations
dataset["""( prog (
    ( setq n ( read 0 ) )
    ( return ( plus 100000 ( minus n 200000 ) ) )
) )"""] = [[[300000], 200000]]

# 6
# check `while`
# Similar to the example from the statement
dataset["""( prog ( 
  ( setq s 0 )
  ( setq j 100 )
  ( while ( nonequal j 0 ) ( 
      ( setq s ( plus s j ) ) 
      ( setq j ( minus j 1 ) )
    ) 
  )
  ( return s ) )
)"""] = [[[], 5050]]

# 7
# check `break`
dataset["""( prog (
    ( setq value 0 )
    ( setq i 0 )  
    ( while ( equal 100000 100000 ) (
        ( cond ( less i 10 )
               ( ( setq value ( plus value ( times i 2 ) ) )
                 ( setq i ( plus i 1 ) ) 
               )
               ( break )
        )
      )
    )
    ( return value ) ) )"""] = [[[], 90]]

# 8
# check `func`
dataset["""( func Cube ( arg ) (
           ( return ( times ( times arg arg ) arg ) ) ) )

( prog ( 
    ( setq value ( read 0 ) )
    ( setq retval ( Cube value ) )
    ( return retval )
  )
)"""] = [[[4], 4 ** 3],
         [[1000], 1000 ** 3]]

# 9
# check `func` without `return`
dataset["""( func Cube ( arg ) ( times ( times arg arg ) arg ) )

( prog ( 
    ( setq value ( read 0 ) )
    ( return ( Cube value ) )
  )
)"""] = [[[4], 4 ** 3],
         [[1000], 1000 ** 3]]

# 10
# check `func` with var
dataset["""( func mod ( a b ) (
    ( setq retval a )
    ( while ( greatereq retval b ) (
        ( setq retval ( minus retval b ) ) 
      )
    )
    ( return retval )
  )
)

( prog (
    ( setq a 123 )
    ( setq b 45 )
    ( return ( mod a b ) )
  )
)"""] = [[[], 123 % 45]]

# 11
# check `and`
dataset["""( prog (
    ( setq a ( read 0 ) )
    ( setq b ( read 1 ) )
    ( cond ( and ( greater a 500 ) ( less a 600 ) )
           ( setq b a )
    )
    ( return b )
  )
)"""] = [[[10, 10], 10],
         [[500, 1], 1],
         [[600, 10000], 10000],
         [[550, 256], 550]]

# 12
# check `or`
dataset["""( prog (
    ( setq a ( read 0 ) )
    ( setq b ( read 1 ) )
    ( cond ( or ( lesseq a 256 ) ( lesseq b 256 ) )
           ( setq b ( times a b ) )
    )
    ( return b )
  )
)"""] = [[[10, 10], 10 * 10],
         [[256, 10000], 256 * 10000],
         [[600, 256], 600 * 256],
         [[999, 999], 999]]

# 13
# check `not`
dataset["""( prog (
    ( setq a ( read 0 ) )
    ( cond ( not ( lesseq a 255 ) )
           ( setq a ( times a a ) )
    )
    ( return a )
  )
)"""] = [[[10], 10],
         [[255], 255],
         [[256], 256 * 256]]

# 14
# check `func` without params
dataset["""( func simple () (
    ( setq a 10 )
    ( setq b 20 )
    ( while ( less a b ) 
            ( setq a ( plus a b ) )
    )
    ( return a )
  )
)

( prog (
    ( return ( simple ) )
  )
)"""] = [[[], 30]]

# 15
# From the statement
dataset["""( func reqsumma ( a ) (
    ( cond ( equal a 0 )
      ( return 0 )
      ( return ( plus a ( reqsumma ( minus a 1 ) ) ) )
    )
  )
)

( prog ( ( return ( reqsumma 200 ) ) ) )"""] = [[[], ((200 + 1) / 2) * 200]]

class Stack:
    def __init__(self, size=1024):

        self.size = size
        self.__stk = []

    def put(self, value: int, index=0):
        """
        In stack all values are uint256.
        To store negative value I use
        toTwosComplement() function
        """

        value = toTwosComplement(value, self.size)
        self.__stk.insert(index, value)

    def popTop(self, sign=False):
        """
        since stack contains only unsign values,
        to get sign value set sign-flag true
        """

        if not self.__stk.__len__():
            raise IndexError

        if sign:
            return fromTwosComplement(self.__stk.pop(0), self.size)
        else:
            return self.__stk.pop(0)

    #
    # The same functions, but without add/remove and with indexes
    #

    def setElement(self, index, value):
        value = toTwosComplement(value, self.size)
        self.__stk[index] = value

    def getElement(self, index, sign=False):
        return None if index >= self.__stk.__len__() \
            else fromTwosComplement(self.__stk[index], self.size) \
            if sign else self.__stk[index]

    #

    def pprint(self):
        messageToPrint = 'Stack: '

        for value in self.__stk:
            if str(value).__len__() > 20:
                messageToPrint += f"| {hex(value)[:10]}..{hex(value)[-10:]} "
            else:
                messageToPrint += f"| {hex(value)} "
        if self.__stk.__len__() == 0:
            messageToPrint += "| EMPTY STACK "
        messageToPrint += "|"

        print(messageToPrint)

    def __len__(self):
        return self.__stk.__len__()


opcodes = {
    0x00: ['STOP', 0, 0, 0],
    0x01: ['ADD', 2, 1, 3],
    0x02: ['MUL', 2, 1, 5],
    0x03: ['SUB', 2, 1, 3],
    0x04: ['DIV', 2, 1, 5],
    0x05: ['SDIV', 2, 1, 5],
    0x06: ['MOD', 2, 1, 5],
    0x07: ['SMOD', 2, 1, 5],
    0x08: ['ADDMOD', 3, 1, 8],
    0x09: ['MULMOD', 3, 1, 8],
    0x0a: ['EXP', 2, 1, 10],
    0x0b: ['SIGNEXTEND', 2, 1, 5],
    0x10: ['LT', 2, 1, 3],
    0x11: ['GT', 2, 1, 3],
    0x12: ['SLT', 2, 1, 3],
    0x13: ['SGT', 2, 1, 3],
    0x14: ['EQ', 2, 1, 3],
    0x15: ['ISZERO', 1, 1, 3],
    0x16: ['AND', 2, 1, 3],
    0x17: ['OR', 2, 1, 3],
    0x18: ['XOR', 2, 1, 3],
    0x19: ['NOT', 1, 1, 3],
    0x1a: ['BYTE', 2, 1, 3],
    0x1b: ['SHL', 2, 1, 3],
    0x1c: ['SHR', 2, 1, 3],
    0x1d: ['SAR', 2, 1, 3],
    0x35: ['CALLDATALOAD', 1, 1, 3],
    0x36: ['CALLDATASIZE', 0, 1, 2],
    0x37: ['CALLDATACOPY', 3, 0, 3],
    0x3d: ['RETURNDATASIZE', 0, 1, 2],
    0x3e: ['RETURNDATACOPY', 3, 0, 3],
    0x50: ['POP', 1, 0, 2],
    0x51: ['MLOAD', 1, 1, 3],
    0x52: ['MSTORE', 2, 0, 3],
    0x53: ['MSTORE8', 2, 0, 3],
    0x54: ['SLOAD', 1, 1, 50],
    0x55: ['SSTORE', 2, 0, 0],
    0x56: ['JUMP', 1, 0, 8],
    0x57: ['JUMPI', 2, 0, 10],
    0x58: ['PC', 0, 1, 2],
    0x59: ['MSIZE', 0, 1, 2],
    0x5b: ['JUMPDEST', 0, 0, 1],
    0xf3: ['RETURN', 2, 0, 0],
    0xf5: ['CALLBLACKBOX', 7, 1, 40],
    0xfa: ['STATICCALL', 6, 1, 40],
    0xfd: ['REVERT', 2, 0, 0],
    0xff: ['SUICIDE', 1, 0, 0]
}

opcodesMetropolis = {0x3d, 0x3e, 0xfa, 0xfd}

for i in range(1, 33):
    opcodes[0x5f + i] = ['PUSH' + str(i), 0, 1, 3]

for i in range(1, 17):
    opcodes[0x7f + i] = ['DUP' + str(i), i, i + 1, 3]
    opcodes[0x8f + i] = ['SWAP' + str(i), i + 1, i + 1, 3]


def safe_ord(value):
    if isinstance(value, int):
        return value
    else:
        return ord(value)


def signInt(integer, limit):
    a = 2 ** limit
    return integer if integer < a else integer - a


def toTwosComplement(value, bits):
    # If value < 0 then inverse bits and add 1
    if value < 0:
        value = 2 ** bits + value

    # Apply the mask 0xFFFF...FF
    return value & (2 ** bits - 1)


def fromTwosComplement(value: int, bits: int):
    if value > 2 ** (bits - 1) - 1:
        return -(2 ** bits - value)
    return value


def fromByteToInt(value: bytes):
    return int(value.hex(), 16)


def extendMemory(memory: bytearray, toIndex: int):
    to_extend = toIndex - memory.__len__()
    memory.extend(b'\x00' * to_extend)
    return memory


def pprintMemory(memory: bytearray, printAll=True):
    memoryHex = memory.hex()
    if memoryHex.__len__() > 20 and not printAll:
        memoryHex = f"{memoryHex[:10]}...{memoryHex[-10:]}"
    print(f"Memory: 0x{memoryHex}")


def fromByteArrayToInt(barray: bytearray):
    return int(barray.hex(), 16)


def printError(message):
    print(f"Error: {message}")
    exit()


def generateInput(params):
    calldata = ''
    for i in params:
        calldata += hex(i)[2:].zfill(64)[-64:]
    message = bytearray(bytes.fromhex(calldata))
    return message


def execute(code: bytearray, stack: Stack, memory: bytearray, storage: dict, msg: bytearray, toprint=False):
    """
    :param code: e.g. b'\0x60\0x01\0x60\0x01\0x01'
    :param stack: Stack() instance
    :param memory: bytearray for memory
    :param storage: dict for storage
    :param msg: msg.data, call data values
    :param toprint: if true then it will print status after each instruction
    """
    pc = 0

    while pc < code.__len__():

        # |60|03|60|04|01|
        #        /\
        #        ||
        #        pc = 0002

        opcode = code[pc]

        if opcode not in opcodes:
            raise KeyError(f"I can not find such opcode '{opcode}'. pc = {pc}")

        opcodename, in_args, out_args, fee = opcodes[opcode]

        if len(stack) < in_args:
            print(f"Error: stack has length {len(stack)} but {opcodename} (PC={pc}) has to have {in_args} input args.")
            return b""

        amountOfArgs = 0
        # -----------
        # 0x00 - 0x10
        # -----------

        if opcodename == "STOP":
            return b""
        elif opcodename == "ADD":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(a + b)
        elif opcodename == "MUL":
            stack.put(stack.popTop(sign=True) * stack.popTop(sign=True))
        elif opcodename == "SUB":
            stack.put(stack.popTop(sign=True) - stack.popTop(sign=True))
        elif opcodename == "DIV":
            a, b = stack.popTop(), stack.popTop()
            stack.put(0 if b == 0 else a // b)
        elif opcodename == "SDIV":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(0 if b == 0 else a // b)
        elif opcodename == "MOD":
            a, b = stack.popTop(), stack.popTop()
            stack.put(0 if b == 0 else a % b)
        elif opcodename == "SMOD":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(0 if b == 0 else a % b)
        elif opcodename == "ADDMOD":
            a, b, N = stack.popTop(sign=True), \
                      stack.popTop(sign=True), \
                      stack.popTop(sign=True)
            stack.put(0 if N == 0 else (a + b) % N)
        elif opcodename == "MULMOD":
            a, b, N = stack.popTop(sign=True), \
                      stack.popTop(sign=True), \
                      stack.popTop(sign=True)
            stack.put(0 if N == 0 else (a * b) % N)
        elif opcodename == "EXP":
            stack.put(stack.popTop() ** stack.popTop())
        elif opcodename == "SIGNEXTEND":
            raise Exception("Opcode is not implemented yet")

        # -----------
        # 0x10 - 0x19
        # -----------

        elif opcodename == "LT":
            stack.put(1 if stack.popTop() < stack.popTop() else 0)
        elif opcodename == "GT":
            stack.put(1 if stack.popTop() > stack.popTop() else 0)
        elif opcodename == "SLT":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(1 if a < b else 0)
        elif opcodename == "SGT":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(1 if a > b else 0)
        elif opcodename == "EQ":
            stack.put(1 if stack.popTop() == stack.popTop() else 0)
        elif opcodename == "ISZERO":
            stack.put(1 if stack.popTop() == 0 else 0)
        elif opcodename == "AND":
            stack.put(stack.popTop() & stack.popTop())
        elif opcodename == "OR":
            stack.put(stack.popTop() | stack.popTop())
        elif opcodename == "XOR":
            stack.put(stack.popTop() ^ stack.popTop())
        elif opcodename == "NOT":
            stack.put(stack.popTop() ^ (2 ** stack.size - 1))

        # -----------
        # 0x1A - 0x1D
        # -----------

        elif opcodename == "BYTE":
            i, x = stack.popTop(), stack.popTop()
            # idk what is it
            stack.put((x >> (248 - i * 8)) & 0xFF)
        elif opcodename == "SHL":
            shift, value = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(value << shift)
        elif opcodename == "SHR":
            shift, value = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(value >> shift)
        elif opcodename == "SAR":
            shift, value = stack.popTop(), stack.popTop()
            stack.put(value >> shift)

        # --------
        #
        # --------

        elif opcodename == "CALLDATALOAD":
            offset = stack.popTop()

            if offset + 32 > len(msg):
                print(
                    f"Error: message data has length {len(msg)} "
                    f"but {opcodename} (PC={pc}) refers to [{offset}:{offset + 32}]")
                return b""
            value = msg[offset:offset + 32]
            stack.put(fromByteArrayToInt(value))
        elif opcodename == "CALLDATASIZE":
            stack.put(len(msg))
        elif opcodename == "CALLDATACOPY":
            destOffset, offset, length = stack.popTop(), stack.popTop(), stack.popTop()
            if destOffset + length > len(memory):
                print(
                    f"Error: memory has length {len(memory)} "
                    f"but {opcodename} (PC={pc}) refers to [{destOffset}:{destOffset + length}]")
                return b""
            if offset + length > len(msg):
                print(
                    f"Error: message data has length {len(msg)} "
                    f"but {opcodename} (PC={pc}) refers to [{offset}:{offset + length}]")
                return b""

            memory[destOffset:destOffset + length] = msg[offset:offset + length]

        # -----------
        # 0x50 - 0x58
        # -----------

        elif opcodename == "POP":
            stack.popTop()
        elif opcodename == "MLOAD":
            offset = stack.popTop()
            if offset + 32 > len(memory):
                stack.put(0)
            else:
                value = memory[offset:offset + 32]
                stack.put(fromByteToInt(value))
        elif opcodename == "MSTORE":
            offset = stack.popTop()
            value = stack.popTop()
            memory = extendMemory(memory, offset)
            memory[offset:offset + 32] = value.to_bytes(32, byteorder='big')
        elif opcodename == "MSTORE8":
            offset = stack.popTop()
            value = stack.popTop()
            memory = extendMemory(memory, offset)
            memory[offset:offset + 32] = (value & 0xFF).to_bytes(32, byteorder='big')
        elif opcodename == "SLOAD":
            key = stack.popTop(sign=True)
            if key in storage:
                value = storage[key]
            else:
                value = 0
            stack.put(value)
        elif opcodename == "SSTORE":
            key = stack.popTop(sign=True)
            value = stack.popTop(sign=True)
            storage[key] = value
        elif opcodename == "JUMP":
            destination = stack.popTop()
            if code[destination] not in opcodes:
                raise Exception(f"Destination {destination} not in opcodes")
            if code[destination] != 0x5b:
                print(f"Error: Destination has to be JUMPDEST [0x5B] but it is {code[destination]}. (PC={pc})")
                return b""
            pc = destination
        elif opcodename == "JUMPI":
            destination, condition = stack.popTop(), stack.popTop()
            if code[destination] not in opcodes:
                raise Exception(f"Destination {destination} not in opcodes")
            if code[destination] != 0x5b:
                print(f"Error: Destination has to be JUMPDEST [0x5B] but it is {code[destination]}. (PC={pc})")
                return b""
            pc = destination if condition else pc + 1
        elif opcodename == "JUMPDEST":
            pass
        elif opcodename == "PC":
            stack.put(pc)

        # -----------
        # 0x60 - 0x9F
        # -----------

        elif opcodename.startswith("PUSH"):
            amountOfArgs = int(opcodename.replace("PUSH", ""))
            value = fromByteToInt(code[pc + 1:pc + amountOfArgs + 1])
            stack.put(value)
            pc += amountOfArgs
        elif opcodename.startswith("DUP"):
            amountOfArgs = int(opcodename.replace("DUP", "")) - 1
            value = stack.getElement(amountOfArgs)
            stack.put(value)
        elif opcodename.startswith("SWAP"):
            amountOfArgs = int(opcodename.replace("SWAP", ""))
            value1, value2 = stack.getElement(0), stack.getElement(amountOfArgs)
            stack.setElement(0, value2)
            stack.setElement(amountOfArgs, value1)

        # -----------
        # 0xF3 - 0xF3
        # -----------

        elif opcodename == "RETURN":
            offset = stack.popTop()
            length = stack.popTop()
            return memory[offset:offset + length]

        else:
            raise Exception(f"We have not such opcode yet {opcodename}")

        if toprint:
            args = code[pc - amountOfArgs + 1:pc + 1].hex()
            args = [args[i:i + 2] for i in range(0, len(args), 2)]
            print(f"[ {opcodename}{' ' if len(args) != 0 else ''}{' '.join(args)} ] has executed.")

            stack.pprint()

            pprintMemory(memory)

            print(f"Storage: {storage}")
            print('-' * 10)

        # If opcode is JUMP or JUMPI instruction, then skip this step
        if not opcodename in ["JUMP", "JUMPI"]:
            pc += 1


def generate():
    tests = []
    for test in dataset:
        tests.append((test, (test, dataset[test])))
    return tests


def check(reply, clue):
    taskId, tests = clue
    count = 0
    for test in tests:
        inpt, outpt = test
        inpt = generateInput(inpt)
        stack = Stack(1024)
        code = bytearray(bytes.fromhex(reply))
        memory = bytearray()
        storage = {}
        ans = execute(code, stack, memory, storage, inpt)
        if ans is None:
            ans = b'0'
        ans = int(ans.hex(), 16)
        if ans != outpt:
            count = count + 1
    if count == 0:
        return True
    else:
        return False, f'{count} of {len(tests)} sub-tests failed'