from Stack import Stack
from opcodes import opcodes
import utils


def execute(code: bytearray, stack: Stack, memory: bytearray, storage: dict, toprint=False):
    """
    :param code: e.g. b'\0x60\0x01\0x60\0x01\0x01'
    :param stack: Stack() instance
    :param memory: bytearray for memory
    :param storage: dict for storage
    :return: bytes if contract has stopped by RETURN
    :param toprint: if true then it will print status after each instruction
    """
    pc = 0

    while pc < code.__len__():

        # |60|03|60|04|01|
        #        /\
        #        ||
        #        pc = 0002

        opcode = code[pc]

        if opcode not in opcodes:
            raise KeyError(f"I can not find such opcode '{opcode}'. pc = {pc}")

        opcodename, in_args, out_args, fee = opcodes[opcode]
        amountOfArgs = 0
        # -----------
        # 0x00 - 0x10
        # -----------

        if opcodename == "STOP":
            return None
        elif opcodename == "ADD":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(a + b)
        elif opcodename == "MUL":
            stack.put(stack.popTop(sign=True) * stack.popTop(sign=True))
        elif opcodename == "SUB":
            stack.put(stack.popTop(sign=True) - stack.popTop(sign=True))
        elif opcodename == "DIV":
            a, b = stack.popTop(), stack.popTop()
            stack.put(0 if b == 0 else a // b)
        elif opcodename == "SDIV":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(0 if b == 0 else a // b)
        elif opcodename == "MOD":
            a, b = stack.popTop(), stack.popTop()
            stack.put(0 if b == 0 else a % b)
        elif opcodename == "SMOD":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(0 if b == 0 else a % b)
        elif opcodename == "ADDMOD":
            a, b, N = stack.popTop(sign=True), \
                      stack.popTop(sign=True), \
                      stack.popTop(sign=True)
            stack.put(0 if N == 0 else (a + b) % N)
        elif opcodename == "MULMOD":
            a, b, N = stack.popTop(sign=True), \
                      stack.popTop(sign=True), \
                      stack.popTop(sign=True)
            stack.put(0 if N == 0 else (a * b) % N)
        elif opcodename == "EXP":
            stack.put(stack.popTop() ** stack.popTop())
        elif opcodename == "SIGNEXTEND":
            raise Exception("Opcode is not implemented yet")

        # -----------
        # 0x10 - 0x19
        # -----------

        elif opcodename == "LT":
            stack.put(1 if stack.popTop() < stack.popTop() else 0)
        elif opcodename == "GT":
            stack.put(1 if stack.popTop() > stack.popTop() else 0)
        elif opcodename == "SLT":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(1 if a < b else 0)
        elif opcodename == "SGT":
            a, b = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(1 if a > b else 0)
        elif opcodename == "EQ":
            stack.put(1 if stack.popTop() == stack.popTop() else 0)
        elif opcodename == "ISZERO":
            stack.put(1 if stack.popTop() == 0 else 0)
        elif opcodename == "AND":
            stack.put(stack.popTop() & stack.popTop())
        elif opcodename == "OR":
            stack.put(stack.popTop() | stack.popTop())
        elif opcodename == "XOR":
            stack.put(stack.popTop() ^ stack.popTop())
        elif opcodename == "NOT":
            stack.put(stack.popTop() ^ (2 ** stack.size - 1))

        # -----------
        # 0x1A - 0x1D
        # -----------

        elif opcodename == "BYTE":
            i, x = stack.popTop(), stack.popTop()
            # idk what is it
            stack.put((x >> (248 - i * 8)) & 0xFF)
        elif opcodename == "SHL":
            shift, value = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(value << shift)
        elif opcodename == "SHR":
            shift, value = stack.popTop(sign=True), stack.popTop(sign=True)
            stack.put(value >> shift)
        elif opcodename == "SAR":
            shift, value = stack.popTop(), stack.popTop()
            stack.put(value >> shift)

        # -----------
        # 0x50 - 0x58
        # -----------

        elif opcodename == "POP":
            stack.popTop()
        elif opcodename == "MLOAD":
            offset = stack.popTop()
            value = memory[offset:offset + 32]
            stack.put(utils.fromByteToInt(value))
        elif opcodename == "MSTORE":
            offset = stack.popTop()
            value = stack.popTop()
            memory = utils.extendMemory(memory, offset)
            memory[offset:offset + 32] = value.to_bytes(32, byteorder='big')
        elif opcodename == "MSTORE8":
            offset = stack.popTop()
            value = stack.popTop()
            memory = utils.extendMemory(memory, offset)
            memory[offset:offset + 32] = (value & 0xFF).to_bytes(32, byteorder='big')
        elif opcodename == "SLOAD":
            key = stack.popTop(sign=True)
            if key in storage:
                value = storage[key]
            else:
                value = 0
            stack.put(value)
        elif opcodename == "SSTORE":
            key = stack.popTop(sign=True)
            value = stack.popTop(sign=True)
            storage[key] = value
        elif opcodename == "JUMP":
            destination = stack.popTop()
            if code[destination] not in opcodes:
                raise Exception(f"Destination {destination} not in opcodes")
            pc = destination
        elif opcodename == "JUMPI":
            destination, condition = stack.popTop(), stack.popTop()
            if code[destination] not in opcodes:
                raise Exception(f"Destination {destination} not in opcodes")
            pc = destination if condition else pc
        elif opcodename == "PC":
            stack.put(pc)

        # -----------
        # 0x60 - 0x9F
        # -----------

        elif opcodename.startswith("PUSH"):
            amountOfArgs = int(opcodename.replace("PUSH", ""))
            value = utils.fromByteToInt(code[pc + 1:pc + amountOfArgs + 1])
            stack.put(value)
            pc += amountOfArgs
        elif opcodename.startswith("DUP"):
            amountOfArgs = int(opcodename.replace("DUP", "")) - 1
            value = stack.getElement(amountOfArgs)
            stack.put(value)
        elif opcodename.startswith("SWAP"):
            amountOfArgs = int(opcodename.replace("SWAP", "")) - 1
            value1, value2 = stack.getElement(0), stack.getElement(amountOfArgs)
            stack.setElement(0, value2)
            stack.setElement(amountOfArgs, value1)

        # -----------
        # 0xF3 - 0xF3
        # -----------

        elif opcodename == "RETURN":
            offset = stack.popTop()
            length = stack.popTop()
            return memory[offset:offset + length]

        else:
            raise

        if toprint:
            args = code[pc - amountOfArgs + 1:pc + 1].hex()
            args = [args[i:i + 2] for i in range(0, len(args), 2)]
            print(f"[ {opcodename}{' ' if len(args) != 0 else ''}{' '.join(args)} ] has executed.")

            stack.pprint()

            utils.pprintMemory(memory)

            print(f"Storage: {storage}")
            print('-' * 10)

        # If opcode is JUMP instruction, then we already
        if not opcodename.startswith("JUMP"):
            pc += 1
