from utils import toTwosComplement, fromTwosComplement


class Stack:
    def __init__(self, size=256):

        self.size = size
        self.__stk = []

    def put(self, value: int, index=0):
        """
        In stack all values are uint256.
        To store negative value I use
        toTwosComplement() function
        """

        value = toTwosComplement(value, self.size)
        self.__stk.insert(index, value)

    def popTop(self, sign=False):
        """
        since stack contains only unsign values,
        to get sign value set sign-flag true
        """

        assert self.__stk.__len__() != 0

        if sign:
            return fromTwosComplement(self.__stk.pop(0), self.size)
        else:
            return self.__stk.pop(0)

    #
    # The same functions, but without add/remove and with indexes
    #

    def setElement(self, index, value):
        value = toTwosComplement(value, self.size)
        self.__stk[index] = value

    def getElement(self, index, sign=False):
        return None if index >= self.__stk.__len__() \
            else fromTwosComplement(self.__stk[index], self.size) \
            if sign else self.__stk[index]

    #

    def pprint(self):
        messageToPrint = 'Stack: '

        for value in self.__stk:
            if str(value).__len__() > 20:
                messageToPrint += f"| {str(value)[:10]}..{str(value)[-10:]} "
            else:
                messageToPrint += f"| {str(value)} "
        if self.__stk.__len__() == 0:
            messageToPrint += "| EMPTY STACK "
        messageToPrint += "|"

        print(messageToPrint)
