from Stack import Stack
from vm import execute

if __name__ == "__main__":
    stack = Stack(256)

    # pseudo code:
    # memory[0x0F:0x0F + 0x20] = 0xFFDD
    # stack.put(memory[0x0F:0x0F + 0x20])

    code = bytearray(b'\x61\xFF\xDD\x60\x0F\x52\x60\x0F\x51\x61\x00\x01\x60\x02\01')
    # code = bytearray(b'\x60\xFF\x60\x00\x56')

    memory = bytearray()
    storage = {}
    ans = execute(code, stack, memory, storage, toprint=True)

