EVM bytecode compiler
====

`vm.py` contain main function *execute()*, which handles binary code with given stack, memory and storage

`vm.py` also uses the following files as supporting ones:

    utils.py
    Stack.py
    opcodes.py
    
*execute()* takes 5 arguments:

+ code - bytearray, which represents a sequence of opcodes. For instance __b"\x60\x01\x60\x02\01"__  will add one and two.
+ stack - instance of Stack(). After the code is executed, you can check the stack value.
+ memory - bytearray. After the code is executed, you can check the memory value.
+ storage - instance of dict. After the code is executed, you can check the storage value.
+ toprint - boolean. If true then it will print status after each instruction.