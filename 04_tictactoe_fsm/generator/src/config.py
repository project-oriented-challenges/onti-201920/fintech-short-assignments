# ------------------
# KEY WORDS FOR JSON
# ------------------

INITIAL = 'initial'
STATES = 'states'
HANDLER = 'handler'
SIGNALS = 'signals'
DEFAULT = 'default'
ACTION = 'action'
NEXT = 'next'
TYPE = 'type'
INPUT = 'input'
IMAGE = 'image'

# ------------------
# TIC TAC TIE CONFIG
# ------------------

HUMAN = -1
COMPUTER = 1

# ----------------
# MESSAGES FOR BOT
# ----------------

DRAW_MESSAGE = 'Draw!'
I_WON_MESSAGE = 'I won!'
YOU_WON_MESSAGE = 'You won!'
TRY_AGAIN_MESSAGE = 'You cannot make such move.'
BOT_SHOULD_NOTICE_MESSAGE = 'Bot should notice cheat.'
BOT_CHEATED_MESSAGE = 'Bot cheated!'
BOT_FINISH_GAME = 'Okey, game has finished.'
BOT_SHOULD_FINISH_GAME = 'Bot should finish game'

