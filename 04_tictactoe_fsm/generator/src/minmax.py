from src.config import *
from math import inf as infinity

# Thanks https://github.com/Cledersonbc/tic-tac-toe-minimax/blob/master/py_version/minimax.py

def evaluate(state):
    """
    :param state: the state of the current board
    :return: 1 if the computer wins
            -1 if the human wins
             0 else
    """
    if wins(state, COMPUTER):
        return 1
    elif wins(state, HUMAN):
        return -1
    else:
        return 0


def wins(state, player):
    """
    :param state: the state of the current board
    :param player: 1 or -1 (human or computer)
    :return: Boolean: Somebody wins
    """
    win_state = [
        [state[0][0], state[0][1], state[0][2]],
        [state[1][0], state[1][1], state[1][2]],
        [state[2][0], state[2][1], state[2][2]],
        [state[0][0], state[1][0], state[2][0]],
        [state[0][1], state[1][1], state[2][1]],
        [state[0][2], state[1][2], state[2][2]],
        [state[0][0], state[1][1], state[2][2]],
        [state[2][0], state[1][1], state[0][2]],
    ]
    if [player, player, player] in win_state:
        return True
    else:
        return False


def checkWinnerFromString(string):
    state = transformString(string)
    if wins(state, HUMAN):
        return HUMAN
    elif wins(state, COMPUTER):
        return COMPUTER
    else:
        return 0


def checkWinnerWithExit(string):
    if checkWinnerFromString(string) != 0:
        if checkWinnerFromString(string) == COMPUTER:
            print(f"JsonBot lose!. {string}")
            exit()
        elif checkWinnerFromString(string) == HUMAN:
            print(f"JsonBot won. {string}")
            exit()

    if empty_cells(transformString(string)).__len__() == 1:
        print(f'Draw. {string}')
        exit()

def gameOver(state):
    """
    :param state: The state of board
    :return:
    """
    return wins(state, HUMAN) or wins(state, COMPUTER)


def empty_cells(state):
    """
    :param state: the state of the current board
    :return: a list of empty cells
    """
    cells = []

    for x, row in enumerate(state):
        for y, cell in enumerate(row):
            if cell == 0:
                cells.append([x, y])

    return cells


def minimax(state, depth, player):
    """
    AI function that choice the best move
    :param state: current state of the board
    :param depth: node index in the tree (0 <= depth <= 9),
    but never nine in this case (see iaturn() function)
    :param player: an human or a computer
    :return: a list with [the best row, best col, best score]
    """
    if player == COMPUTER:
        best = [-1, -1, -infinity]
    else:
        best = [-1, -1, +infinity]

    if depth == 0 or gameOver(state):
        score = evaluate(state)
        return [-1, -1, score]

    for cell in empty_cells(state):
        x, y = cell[0], cell[1]
        state[x][y] = player
        score = minimax(state, depth - 1, -player)
        state[x][y] = 0
        score[0], score[1] = x, y

        if player == COMPUTER:
            if score[2] > best[2]:
                best = score  # max value
        else:
            if score[2] < best[2]:
                best = score  # min value

    return best


def reversedMiniMax(state, depth, player):
    """
    AI function that choice the best move
    :param state: current state of the board
    :param depth: node index in the tree (0 <= depth <= 9),
    but never nine in this case (see iaturn() function)
    :param player: an human or a computer
    :return: a list with [the best row, best col, best score]
    """
    if player == COMPUTER:
        best = [-1, -1, +infinity]
    else:
        best = [-1, -1, -infinity]

    if depth == 0 or gameOver(state):
        score = evaluate(state)
        return [-1, -1, score]

    for cell in empty_cells(state):
        x, y = cell[0], cell[1]
        state[x][y] = player
        score = minimax(state, depth - 1, -player)
        state[x][y] = 0
        score[0], score[1] = x, y

        if player == COMPUTER:
            if score[2] < best[2]:
                best = score  # max value
        else:
            if score[2] > best[2]:
                best = score  # min value

    return best


def transformString(string):
    if type(string) != type(str()):
        adasda = 1
    """
    :param string: 000020010 : 1 - X ; 2 - O
    :return: list of current state
    """
    state = []
    for i, char in enumerate(string):
        if not i % 3:
            state.append([])
        state[len(state) - 1].append(int(char.replace('2', '-1')))
    return state


def transformState(state):
    return (''.join(map(str, state[0])) + ''.join(map(str, state[1])) + ''.join(map(str, state[2]))).replace('-1', '2')


def makeMove(string, player):
    state = transformString(string)
    depth = len(empty_cells(state))
    for cell in empty_cells(state):
        x, y = cell

        state[x][y] = player
        if wins(state, player):
            return transformState(state)

        state[x][y] = -player
        if wins(state, -player):
            state[x][y] = player
            return transformState(state)

        state[x][y] = 0

    x, y, _ = minimax(state, depth, player)
    state[x][y] = player

    return transformState(state)


def makeLoseMove(string, player):
    state = transformString(string)
    depth = len(empty_cells(state))

    badMoves = []
    for cell in empty_cells(state):
        x, y = cell
        state[x][y] = -player
        if wins(state, -player):
            badMoves.append([x,y])
            state[x][y] = 0
            #x, y, _ = makeAnyMoveExceptThis(state, x, y)
            #state[x][y] = player
            #return transformState(state)

        state[x][y] = player

        if wins(state, player):
            badMoves.append([x,y])
            state[x][y] = 0
            #x, y, _ = makeAnyMoveExceptThis(state, x, y)
            #state[x][y] = player
            #return transformState(state)

        state[x][y] = 0

    if badMoves.__len__() > 0:
        x, y, _ = makeAnyMoveExceptThis(state, badMoves)
        state[x][y] = player
        return transformState(state)

    x, y, _ = reversedMiniMax(state, depth, player)
    state[x][y] = player
    return transformState(state)

def makeAnyMoveExceptThis(state, badMoves):
    #print(state, _x, _y)
    for cell in empty_cells(state):
        x, y = cell
        if [x, y] not in badMoves:
            return x, y, -1
    return badMoves[0] + [-1]

if __name__ == '__main__':
    string = "110" \
             "110" \
             "000"
    state = transformString(string)
    import src.tools

    print(src.tools.pprintState(makeLoseMove(string, 1)))


    #print(reversedMiniMax(state, 8, 1))

    #print(makeMove('200000110', -1))
