from src.config import *
from src.minmax import makeMove
import json


def createMoves(state, player):
    lst = []
    for i in range(len(state)):
        if state[i] == '0':
            lst.append(state[:i] + str(player).replace('-1', '2') + state[i + 1:])
    return lst

# Initialize dct firstly.
# dct - json variable
dct = {INITIAL: '000'
                '000'
                '010', STATES: {}}

finish = 'finish'
dct[STATES][finish] = {}
dct[STATES][finish][SIGNALS] = {}
dct[STATES][finish][HANDLER] = ""
dct[STATES][finish][SIGNALS][DEFAULT] = {ACTION: BOT_SHOULD_FINISH_GAME,
                                        NEXT: dct[INITIAL]}

state = dct[INITIAL]

dct[STATES][state] = {}
dct[STATES][state][HANDLER] = ""
dct[STATES][state][SIGNALS] = {}

allSignals = createMoves(state, 2)
for signal in allSignals:

    # Make move from bot player
    move = '1' * 9

    _next = finish

    dct[STATES][state][SIGNALS][signal] = {ACTION: {TYPE: IMAGE, HANDLER: "", INPUT: move},
                                           NEXT: _next}


dct[STATES][state][SIGNALS][DEFAULT] = {ACTION: BOT_CHEATED_MESSAGE,
                                        NEXT: state}


with open("tests/human6.json", "w") as write_file:
    json.dump(dct, write_file)
