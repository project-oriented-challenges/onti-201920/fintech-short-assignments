from src.config import *
from src.minmax import makeMove
import json


def createMoves(state, player):
    lst = []
    for i in range(len(state)):
        if state[i] == '0':
            lst.append(state[:i] + str(player).replace('-1', '2') + state[i + 1:])
    return lst

# Initialize dct firstly.
# dct - json variable
dct = {INITIAL: '000'
                '000'
                '100', STATES: {}}

wrong = 'wrong'
dct[STATES][wrong] = {}
dct[STATES][wrong][SIGNALS] = {}
dct[STATES][wrong][HANDLER] = ""
dct[STATES][wrong][SIGNALS][DEFAULT] = {ACTION: BOT_SHOULD_NOTICE_MESSAGE,
                                        NEXT: dct[INITIAL]}

state = dct[INITIAL]

dct[STATES][state] = {}
dct[STATES][state][HANDLER] = ""
dct[STATES][state][SIGNALS] = {}

allSignals = createMoves(state, 2)
for signal in allSignals:

    # Make move from bot player
    move = makeMove(signal, 2)

    _next = wrong

    dct[STATES][state][SIGNALS][signal] = {ACTION: {TYPE: IMAGE, HANDLER: "", INPUT: move},
                                           NEXT: _next}


dct[STATES][state][SIGNALS][DEFAULT] = {ACTION: BOT_CHEATED_MESSAGE,
                                        NEXT: state}


with open("tests/human5.json", "w") as write_file:
    json.dump(dct, write_file)
