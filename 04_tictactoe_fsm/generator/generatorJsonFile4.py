from src.config import *
from src.minmax import checkWinnerFromString
import json

def createMoves(state, player):
    lst = []
    for i in range(len(state)):
        if state[i] == '0':
            lst.append(state[:i] + str(player).replace('-1', '2') + state[i + 1:])
    return lst


# Initialize dct firstly.
# dct - json variable
dct = {INITIAL: '100'
                '000'
                '000', STATES: {}}

wrong = 'wrong'
dct[STATES][wrong] = {}
dct[STATES][wrong][SIGNALS] = {}
dct[STATES][wrong][HANDLER] = ""
dct[STATES][wrong][SIGNALS][DEFAULT] = {ACTION: BOT_SHOULD_NOTICE_MESSAGE,
                                        NEXT: dct[INITIAL]}

state = dct[INITIAL]

dct[STATES][state] = {}
dct[STATES][state][HANDLER] = ""
dct[STATES][state][SIGNALS] = {}

allSignals = createMoves(state, 2)

for signal in allSignals:

    if checkWinnerFromString(signal) == -1:
        dct[STATES][state][SIGNALS][signal] = {ACTION: YOU_WON_MESSAGE,
                                               NEXT: dct[INITIAL]}
        continue

    # ------------------ MAGIC CODE ------------------
    # remove any '1' and add '11' to the end of string
    prev_signal = signal
    signal = signal.replace('1', '0')
    index = signal.find('2')
    if index < 7:
        signal = signal[:7] + '11'
    elif index == 7:
        signal = signal[:6] + '121'
    else:
        signal = signal[:6] + '112'
    # ------------------------------------------------

    move = signal
    _next = wrong

    dct[STATES][state][SIGNALS][prev_signal] = {ACTION: {TYPE: IMAGE, HANDLER: "", INPUT: move},
                                                NEXT: _next}

dct[STATES][state][SIGNALS][DEFAULT] = {ACTION: BOT_CHEATED_MESSAGE,
                                        NEXT: state}



with open("tests/human4.json", "w") as write_file:
    json.dump(dct, write_file)
