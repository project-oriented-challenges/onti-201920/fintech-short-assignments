from src.config import *
from src.minmax import makeMove, checkWinnerFromString, empty_cells, transformString
import json


def createMoves(state, player):
    lst = []
    for i in range(len(state)):
        if state[i] == '0':
            lst.append(state[:i] + str(player).replace('-1', '2') + state[i + 1:])
    return lst

# Initialize dct firstly.
# dct - json variable
dct = {INITIAL: '000'
                '000'
                '001', STATES: {}}

won = 'won'
dct[STATES][won] = {}
dct[STATES][won][SIGNALS] = {}
dct[STATES][won][HANDLER] = ""
dct[STATES][won][SIGNALS][DEFAULT] = {ACTION: I_WON_MESSAGE,
                                      NEXT: dct[INITIAL]}

draw = 'draw'
dct[STATES][draw] = {}
dct[STATES][draw][SIGNALS] = {}
dct[STATES][draw][HANDLER] = ""
dct[STATES][draw][SIGNALS][DEFAULT] = {ACTION: DRAW_MESSAGE,
                                       NEXT: dct[INITIAL]}

# Implementation via stack
stack = [dct[INITIAL]]
while stack:
    state = stack.pop()

    dct[STATES][state] = {}
    dct[STATES][state][HANDLER] = ""
    dct[STATES][state][SIGNALS] = {}

    allSignals = createMoves(state, 2)

    for signal in allSignals:

        move = makeMove(signal, 1)

        if checkWinnerFromString(move) == 1:
            _next = won

        elif empty_cells(transformString(move)).__len__() == 0:
            _next = draw

        else:
            _next = move

        dct[STATES][state][SIGNALS][signal] = {ACTION: {TYPE: IMAGE, HANDLER: "", INPUT: move},
                                               NEXT: _next}
        if _next == move:
            stack.append(move)

    dct[STATES][state][SIGNALS][DEFAULT] = {ACTION: BOT_CHEATED_MESSAGE,
                                            NEXT: state}



with open("tests/human2.json", "w") as write_file:
    json.dump(dct, write_file)
