import sys
sys.path.insert(1, '../generator/')
from src.config import *
from src.minmax import makeMove, checkWinnerFromString, empty_cells, transformString
import json


def createMoves(state, player):
    lst = []
    for i in range(len(state)):
        if state[i] == '0':
            lst.append(state[:i] + str(player).replace('-1', '2') + state[i + 1:])
    return lst


# Default initialize
# dct - json variable
dct = {INITIAL: '0' * 9,
       STATES: {}}

won = 'won'
dct[STATES][won] = {}
dct[STATES][won][SIGNALS] = {}
dct[STATES][won][HANDLER] = ""
dct[STATES][won][SIGNALS][DEFAULT] = {ACTION: I_WON_MESSAGE,
                                      NEXT: dct[INITIAL]}

draw = 'draw'
dct[STATES][draw] = {}
dct[STATES][draw][SIGNALS] = {}
dct[STATES][draw][HANDLER] = ""
dct[STATES][draw][SIGNALS][DEFAULT] = {ACTION: DRAW_MESSAGE,
                                       NEXT: dct[INITIAL]}
# Implementation via stack
stack = [dct[INITIAL]]
while stack:
    # Take a state from stack after bot move. State - string with len 9.
    state = stack.pop()

    dct[STATES][state] = {}
    dct[STATES][state][HANDLER] = ""
    dct[STATES][state][SIGNALS] = {}

    # All variants how human could move.
    allSignals = createMoves(state, 1)

    for signal in allSignals:

        # If Human move lead to draw, then send DRAW_MESSAGE and go to initial state
        if empty_cells(transformString(signal)).__len__() == 0:
            _next = draw
            dct[STATES][state][SIGNALS][signal] = {ACTION: DRAW_MESSAGE,
                                                   NEXT: dct[INITIAL]}
            continue

        # Ask minimax how to move.
        move = makeMove(signal, -1)

        # If bot won, then next state in win state
        if checkWinnerFromString(move) == -1:
            _next = won
        else:
            _next = move

        dct[STATES][state][SIGNALS][signal] = {ACTION: {"type": "image", "handler": "", "input": move},
                                               NEXT: _next}

        stack.append(move)

    # In each state bot should have a finish variant
    dct[STATES][state][SIGNALS]['1' * 9] = {ACTION: BOT_FINISH_GAME,
                                            NEXT: dct[INITIAL]}
    # In each state bot should have a DEFAULT signal
    dct[STATES][state][SIGNALS][DEFAULT] = {ACTION: TRY_AGAIN_MESSAGE,
                                            NEXT: state}

# Write to the json file
with open("code.json", "w") as write_file:
    json.dump(dct, write_file)
