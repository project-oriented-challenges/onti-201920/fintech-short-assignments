from math import inf as infinity
import json

# ------------------
# KEY WORDS FOR JSON
# ------------------

INITIAL = 'initial'
STATES = 'states'
HANDLER = 'handler'
SIGNALS = 'signals'
DEFAULT = 'default'
ACTION = 'action'
NEXT = 'next'
TYPE = 'type'
INPUT = 'input'
IMAGE = 'image'

# ------------------
# TIC TAC TIE CONFIG
# ------------------

HUMAN = -1
COMPUTER = 1

# ----------------
# MESSAGES FOR BOT
# ----------------

DRAW_MESSAGE = 'Draw!'
I_WON_MESSAGE = 'I won!'
YOU_WON_MESSAGE = 'You won!'
TRY_AGAIN_MESSAGE = 'You cannot make such move.'
BOT_SHOULD_NOTICE_MESSAGE = 'Bot should notice cheat.'
BOT_CHEATED_MESSAGE = 'Bot cheated!'
BOT_FINISH_GAME = 'Okey, game has finished.'
BOT_SHOULD_FINISH_GAME = 'Bot should finish game'

# Thanks https://github.com/Cledersonbc/tic-tac-toe-minimax/blob/master/py_version/minimax.py


def evaluate(state):
  """
    :param state: the state of the current board
    :return: 1 if the computer wins
            -1 if the human wins
             0 else
    """
  if wins(state, COMPUTER):
    return 1
  elif wins(state, HUMAN):
    return -1
  else:
    return 0


def wins(state, player):
  """
    :param state: the state of the current board
    :param player: 1 or -1 (human or computer)
    :return: Boolean: Somebody wins
    """
  win_state = [
      [state[0][0], state[0][1], state[0][2]],
      [state[1][0], state[1][1], state[1][2]],
      [state[2][0], state[2][1], state[2][2]],
      [state[0][0], state[1][0], state[2][0]],
      [state[0][1], state[1][1], state[2][1]],
      [state[0][2], state[1][2], state[2][2]],
      [state[0][0], state[1][1], state[2][2]],
      [state[2][0], state[1][1], state[0][2]],
  ]
  if [player, player, player] in win_state:
    return True
  else:
    return False


def checkWinnerFromString(string):
  state = transformString(string)
  if wins(state, HUMAN):
    return HUMAN
  elif wins(state, COMPUTER):
    return COMPUTER
  else:
    return 0


def checkWinnerWithExit(string):
  if checkWinnerFromString(string) != 0:
    if checkWinnerFromString(string) == COMPUTER:
      print(f"JsonBot lose!. {string}")
      exit()
    elif checkWinnerFromString(string) == HUMAN:
      print(f"JsonBot won. {string}")
      exit()

  if empty_cells(transformString(string)).__len__() == 1:
    print(f'Draw. {string}')
    exit()


def gameOver(state):
  """
    :param state: The state of board
    :return:
    """
  return wins(state, HUMAN) or wins(state, COMPUTER)


def empty_cells(state):
  """
    :param state: the state of the current board
    :return: a list of empty cells
    """
  cells = []

  for x, row in enumerate(state):
    for y, cell in enumerate(row):
      if cell == 0:
        cells.append([x, y])

  return cells


def minimax(state, depth, player):
  """
    AI function that choice the best move
    :param state: current state of the board
    :param depth: node index in the tree (0 <= depth <= 9),
    but never nine in this case (see iaturn() function)
    :param player: an human or a computer
    :return: a list with [the best row, best col, best score]
    """
  if player == COMPUTER:
    best = [-1, -1, -infinity]
  else:
    best = [-1, -1, +infinity]

  if depth == 0 or gameOver(state):
    score = evaluate(state)
    return [-1, -1, score]

  for cell in empty_cells(state):
    x, y = cell[0], cell[1]
    state[x][y] = player
    score = minimax(state, depth - 1, -player)
    state[x][y] = 0
    score[0], score[1] = x, y

    if player == COMPUTER:
      if score[2] > best[2]:
        best = score  # max value
    else:
      if score[2] < best[2]:
        best = score  # min value

  return best


def reversedMiniMax(state, depth, player):
  """
    AI function that choice the best move
    :param state: current state of the board
    :param depth: node index in the tree (0 <= depth <= 9),
    but never nine in this case (see iaturn() function)
    :param player: an human or a computer
    :return: a list with [the best row, best col, best score]
    """
  if player == COMPUTER:
    best = [-1, -1, +infinity]
  else:
    best = [-1, -1, -infinity]

  if depth == 0 or gameOver(state):
    score = evaluate(state)
    return [-1, -1, score]

  for cell in empty_cells(state):
    x, y = cell[0], cell[1]
    state[x][y] = player
    score = minimax(state, depth - 1, -player)
    state[x][y] = 0
    score[0], score[1] = x, y

    if player == COMPUTER:
      if score[2] < best[2]:
        best = score  # max value
    else:
      if score[2] > best[2]:
        best = score  # min value

  return best


def transformString(string):
  if type(string) != type(str()):
    adasda = 1
  """
    :param string: 000020010 : 1 - X ; 2 - O
    :return: list of current state
    """
  state = []
  for i, char in enumerate(string):
    if not i % 3:
      state.append([])
    state[len(state) - 1].append(int(char.replace('2', '-1')))
  return state


def transformState(state):
  return (''.join(map(str, state[0])) + ''.join(map(str, state[1])) +
          ''.join(map(str, state[2]))).replace('-1', '2')


def makeMove(string, player):
  state = transformString(string)
  depth = len(empty_cells(state))
  for cell in empty_cells(state):
    x, y = cell

    state[x][y] = player
    if wins(state, player):
      return transformState(state)

    state[x][y] = -player
    if wins(state, -player):
      state[x][y] = player
      return transformState(state)

    state[x][y] = 0

  x, y, _ = minimax(state, depth, player)
  state[x][y] = player

  return transformState(state)


def makeLoseMove(string, player):
  state = transformString(string)
  depth = len(empty_cells(state))

  badMoves = []
  for cell in empty_cells(state):
    x, y = cell
    state[x][y] = -player
    if wins(state, -player):
      badMoves.append([x, y])
      state[x][y] = 0
      #x, y, _ = makeAnyMoveExceptThis(state, x, y)
      #state[x][y] = player
      #return transformState(state)

    state[x][y] = player

    if wins(state, player):
      badMoves.append([x, y])
      state[x][y] = 0
      #x, y, _ = makeAnyMoveExceptThis(state, x, y)
      #state[x][y] = player
      #return transformState(state)

    state[x][y] = 0

  if badMoves.__len__() > 0:
    x, y, _ = makeAnyMoveExceptThis(state, badMoves)
    state[x][y] = player
    return transformState(state)

  x, y, _ = reversedMiniMax(state, depth, player)
  state[x][y] = player
  return transformState(state)


def makeAnyMoveExceptThis(state, badMoves):
  #print(state, _x, _y)
  for cell in empty_cells(state):
    x, y = cell
    if [x, y] not in badMoves:
      return x, y, -1
  return badMoves[0] + [-1]


def createMoves(state, player):
  lst = []
  for i in range(len(state)):
    if state[i] == '0':
      lst.append(state[:i] + str(player).replace('-1', '2') + state[i + 1:])
  return lst


# Default initialize
# dct - json variable
dct = {INITIAL: '0' * 9, STATES: {}}

won = 'won'
dct[STATES][won] = {}
dct[STATES][won][SIGNALS] = {}
dct[STATES][won][HANDLER] = ""
dct[STATES][won][SIGNALS][DEFAULT] = {
    ACTION: I_WON_MESSAGE,
    NEXT: dct[INITIAL]
}

draw = 'draw'
dct[STATES][draw] = {}
dct[STATES][draw][SIGNALS] = {}
dct[STATES][draw][HANDLER] = ""
dct[STATES][draw][SIGNALS][DEFAULT] = {
    ACTION: DRAW_MESSAGE,
    NEXT: dct[INITIAL]
}
# Implementation via stack
stack = [dct[INITIAL]]
while stack:
  # Take a state from stack after bot move. State - string with len 9.
  state = stack.pop()

  dct[STATES][state] = {}
  dct[STATES][state][HANDLER] = ""
  dct[STATES][state][SIGNALS] = {}

  # All variants how human could move.
  allSignals = createMoves(state, 1)

  for signal in allSignals:

    # If Human move lead to draw, then send DRAW_MESSAGE and go to initial state
    if empty_cells(transformString(signal)).__len__() == 0:
      _next = draw
      dct[STATES][state][SIGNALS][signal] = {
          ACTION: DRAW_MESSAGE,
          NEXT: dct[INITIAL]
      }
      continue

    # Ask minimax how to move.
    move = makeMove(signal, -1)

    # If bot won, then next state in win state
    if checkWinnerFromString(move) == -1:
      _next = won
    else:
      _next = move

    dct[STATES][state][SIGNALS][signal] = {
        ACTION: {
            "type": "image",
            "handler": "",
            "input": move
        },
        NEXT: _next
    }

    stack.append(move)

  # In each state bot should have a finish variant
  dct[STATES][state][SIGNALS]['1' * 9] = {
      ACTION: BOT_FINISH_GAME,
      NEXT: dct[INITIAL]
  }
  # In each state bot should have a DEFAULT signal
  dct[STATES][state][SIGNALS][DEFAULT] = {
      ACTION: TRY_AGAIN_MESSAGE,
      NEXT: state
  }

# Write to the json file
with open("code.json", "w") as write_file:
  json.dump(dct, write_file)
