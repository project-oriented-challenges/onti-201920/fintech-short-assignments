import random

max_groups = 4

group_max_goods = 3

good_max_price = 100
good_max_count = 100

min_sale_buy_ops = 5
max_sale_buy_ops = 50

min_goods_per_sale_buy = 10
max_goods_per_sale_buy = 20

class Good:
    @staticmethod
    def gen(required_cost = None):
        if(required_cost is None):
            return Good(random.randint(1, good_max_price), 
                        random.randint(1, good_max_count))
        
        count = random.randint(1, good_max_count)
        
        while len(str(required_cost / count)) > 10:
            count = random.randint(1, good_max_count)
            
        return Good(required_cost / count, count)
    
    @staticmethod
    def gen_special():
        p_one = Good.gen()
        
        price = random.randint(1, good_max_price)
        count = random.randint(1, p_one.count) if p_one.price - price >= 0 else random.randint(p_one.count, good_max_count)
        
        return (p_one, Good(price, count))
    
    def __init__(self, price, count, id = None, idInGroup = None, groupID = None, ops = None):
        self.price = float(price)
        self.count = int(count)
        
        self.id = hash(self) if id is None else int(id)
        self.idInGroup = idInGroup
        self.groupID = groupID
        
        self.ops = ops
        
    def __str__(self):
        return 'Good(p: % 10.4f  c: %-3i, id:% 5s, idInGroup: % 5s, groupID: % 5s, ops: % 5s)' % (self.price, self.count, self.id, self.idInGroup, self.groupID, len(self.ops) if not self.ops is None else None)
        
    def get_cost(self):
        return self.price * self.count
        
    def check_ops(self):
        if self.ops is None:
            raise ValueError('ops is None')

        count = 0
        price = 0
        
        for op in self.ops:
            if 'price' in op.keys():
                price = op['price']
                count += op['count']
            else:
                count -= op['count']
                
        return count == self.count  and price == self.price
            
class Group:
    @staticmethod
    def gen(max_cost):
        gp = Group()
        
        for i in range(group_max_goods):
            gp.goods.append(Good.gen())
              
            max_cost -= gp.goods[-1].get_cost()

            if max_cost <= 0:
                gp.goods.pop()
                break
            
        return gp
        
    def gen_special():
        gp_A = Group()
        gp_B = Group()
        
        gp_A.goods.append(Good.gen_special())
        gp_B.goods.append(Good.gen_special())
        
        for i in range(random.randint(1, group_max_goods)):
            good_A = Good.gen()
            good_B = Good.gen(good_A.get_cost())
            
            gp_A.goods.append(good_A)
            gp_B.goods.append(good_B)
        
        return (gp_A, gp_B)
        
    def __init__(self, id = None):
        self.goods = []
        
        self.id = hash(self) if id is None else int(id)
        
    def __str__(self):
        return 'Group(%i items, c:% 10.3f, id: %s): \n\t' % (len(self.goods), self.get_cost(), self.id) + '\n\t'.join([str(i) if type(i) == Good else ('\t' + '\n\t\t'.join([str(z) for z in i])) for i in self.goods]) + '\n'
        
    def get_cost(self):
        return sum([i.get_cost() if type(i) == Good else sum([z.get_cost() for z in i]) for i in self.goods])

def gen_all_groups():
    groups = list(Group.gen_special())
    
    max_cost = (groups[0].get_cost(), groups[1].get_cost())
    
    ans = 0 if max_cost[0] > max_cost[1] else 1
    
    max_cost = max_cost[ans]
    
    for i in range(random.randint(0, max_groups - 2)):
        groups.append(Group.gen(max_cost))
    
    return (ans, groups)

def gen_sequence():
    def get_random_terms(n, l_bound, u_bound):
        nums = []
    
        while n > 0:
            nums.append(random.randint(l_bound, u_bound))
            n -= nums[-1]
    
        nums[nums.index(max(nums))] += n
    
        return nums
    
    size = random.randint(min_sale_buy_ops, max_sale_buy_ops)
    
    buys = get_random_terms(size, min_goods_per_sale_buy, max_goods_per_sale_buy)
    sale = get_random_terms(size, min_goods_per_sale_buy, max_goods_per_sale_buy)
    
    sale = list(map(lambda v : -v, sale))
    
    seq = [buys.pop(), ]
    
    while len(buys) > 0 or len(sale) > 0:
        if(sum(seq) + sale[-1] >= 0):
            seq.append(sale.pop())
        else:
            seq.append(buys.pop())
    
    return seq
    
def gen_raw_date():
    def get_groups():
        #Получаем группы.
        win_indx, groups = gen_all_groups()
        
        #Запоминаем и удаляем группу с максимальной стоимостью.
        win_group = groups.pop(win_indx)
        
        #Перемешиваем.
        random.shuffle(groups)
        
        #Генерируем случайный индекс.
        win_indx = random.randint(0, len(groups) - 1)
        
        #Вставляем по этому индексу.
        groups = groups[:win_indx] + [win_group, ] + groups[win_indx:]
        
        #Индекс становится id.
        win_indx += 1
        
        #Раздаём id всем группам.
        for i in range(len(groups)):
            groups[i].id = i + 1
            
        return (win_indx, groups)
    
    def get_goods(groups):
        #Отделяем товары от групп.
        goods = []
        
        #Раздаём groupID и idInGroup всем товарам.
        for group in groups:
            random.shuffle(group.goods)
        
            for good_i in range(len(group.goods)):
                if type(group.goods[good_i]) == tuple:
                    group.goods[good_i][0].groupID = group.id
                    group.goods[good_i][1].groupID = group.id
                    
                    group.goods[good_i][0].idInGroup = good_i + 1
                    group.goods[good_i][1].idInGroup = good_i + 1
                else:
                    group.goods[good_i].groupID = group.id
                    group.goods[good_i].idInGroup = good_i + 1

                goods.append(group.goods[good_i])
        
        #Перемешиваем.
        random.shuffle(goods)
        
        #Раздаём id товарам.
        for i in range(len(goods)):
            if type(goods[i]) == tuple:
                    goods[i][0].id = i + 1
                    goods[i][1].id = i + 1
            else:
                goods[i].id = i + 1
                
        return goods
    
    def gen_operations(goods):
        def gen_op(good):
            #Эти операции в сумме дают 0.
            ops = [{'price': float(random.randint(1, good_max_price)), 'count': i} if i > 0 else {'count': -i}  for i in gen_sequence()]
            
            #Находим последнюю операцию покупки и меняем цену и количество согласно остатку.
            for op_i in range(len(ops) - 1, -1, -1):
                if not 'price' in ops[op_i].keys():
                    continue
                
                ops[op_i]['price'] = good.price
                ops[op_i]['count'] += good.count
                
                return ops
        
        #Генерируем операции для всех товаров.
        for good in goods:
            if type(good) == tuple:            
                good[0].ops = gen_op(good[0])
                #good[1].ops = gen_op(good[1])
                good[1].ops = [{'price': good[1].price, 'count': good[1].count}]
                
                if(not good[0].check_ops() or not good[1].check_ops()):
                    raise ValueError('Fail')
            else:
                good.ops = gen_op(good)
                
                if(not good.check_ops()):
                    raise ValueError('Fail')
        
        
        
    #Перемешенные группы.
    win_indx, groups = get_groups()
    
    #Другое представление данных. Товары. 
    goods = get_goods(groups)
    
    gen_operations(goods)
    
    return (win_indx, groups)
    
def gen_sql():
    def gen_ggroups_table(groups):
        s = 'CREATE TABLE ggroups (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20));\n'
        
        for group in groups:
            s += "INSERT INTO ggroups (name) VALUES ('GROUP %s');\n" % group.id
        
        return s
    
    def gen_goods_table(goods):
        s = 'CREATE TABLE goods (id INT PRIMARY KEY AUTO_INCREMENT, group_id INT DEFAULT 0, name VARCHAR(20));\n'
        
        for good in goods:
            if type(good) == tuple:
                good = good[0]
                
            s += "INSERT INTO goods (group_id, name) VALUES (%s, 'goods %s.%s');\n" % (good.groupID, good.groupID, good.idInGroup)
        
        return s
     
    def gen_acquisition_sales_tables(goods):
        acqon = 'CREATE TABLE acquisition (TIMESTAMP INT PRIMARY KEY AUTO_INCREMENT, good_id INT, volume INT, price DOUBLE(16,2));\n'
        sales = 'CREATE TABLE sales (TIMESTAMP INT PRIMARY KEY AUTO_INCREMENT, good_id INT, volume INT);\n'
        
            
        indexes = [i for i in range(len(goods))]
            
        while len(indexes) > 0:
            ind = random.choice(indexes)
            
            good = goods[ind]

            if type(good) == tuple:
                good = good[0]
                
                op = good.ops.pop(0)
                
                if 'price' in op.keys():
                    acqon += "INSERT INTO acquisition (good_id, volume, price) VALUES (%s, %s, %s);\n" % (good.id, op['count'], op['price'])
                else:
                    sales += "INSERT INTO sales (good_id, volume) VALUES (%s, %s);\n" % (good.id, op['count'])
                
                if(len(good.ops) == 0):
                    goods[ind] = goods[ind][1]
                    
            else:
                op = good.ops.pop(0)
                
                if 'price' in op.keys():
                    acqon += "INSERT INTO acquisition (good_id, volume, price) VALUES (%s, %s, %s);\n" % (good.id, op['count'], op['price'])
                else:
                    sales += "INSERT INTO sales (good_id, volume) VALUES (%s, %s);\n" % (good.id, op['count'])
            
                if(len(good.ops) == 0):
                    indexes.remove(ind)
                
        
        return (acqon, sales)
     
    win_group, groups = gen_raw_date()
    
    goods = []
    
    for group in groups:
        for good in group.goods:
            goods.append(good)
           
    goods.sort(key = lambda v : (v[0].id if type(v) == tuple else v.id))
           
    #for i in groups:
    #    print(i)
        
    return (win_group, gen_ggroups_table(groups) + gen_goods_table(goods) + ''.join(gen_acquisition_sales_tables(goods)))
        
   
if __name__ == '__main__':
    answer, sql = gen_sql()

    print('Right answer:', 'GROUP', answer)
    print(sql)