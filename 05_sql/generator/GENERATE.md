## Генерация тестовой таблицы

Для создания таблицы используется файл `datagen.py`, результатом работы которого 
будет набор `SQL` команд для создания таблицы с набором входных данных, а также 
правильный ответ для данного сгенерированного теста.

## Пример работы

```
> python .\datagen.py

Right answer: GROUP 1
CREATE TABLE ggroups (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20));
INSERT INTO ggroups (name) VALUES ('GROUP 1');
INSERT INTO ggroups (name) VALUES ('GROUP 2');
CREATE TABLE goods (id INT PRIMARY KEY AUTO_INCREMENT, group_id INT DEFAULT 0, name VARCHAR(20));
INSERT INTO goods (group_id, name) VALUES (2, 'goods 2.1');
INSERT INTO goods (group_id, name) VALUES (1, 'goods 1.2');
INSERT INTO goods (group_id, name) VALUES (1, 'goods 1.1');
INSERT INTO goods (group_id, name) VALUES (2, 'goods 2.3');
INSERT INTO goods (group_id, name) VALUES (1, 'goods 1.3');
INSERT INTO goods (group_id, name) VALUES (2, 'goods 2.2');
CREATE TABLE acquisition (TIMESTAMP INT PRIMARY KEY AUTO_INCREMENT, good_id INT, volume INT, price DOUBLE(16,2));
INSERT INTO acquisition (good_id, volume, price) VALUES (2, 25, 212.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (6, 98, 53.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (1, 97, 94.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (4, 98, 49.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (5, 10, 1128.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (3, 54, 24.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (4, 20, 34.0);
INSERT INTO acquisition (good_id, volume, price) VALUES (3, 92, 92.0);
CREATE TABLE sales (TIMESTAMP INT PRIMARY KEY AUTO_INCREMENT, good_id INT, volume INT);
INSERT INTO sales (good_id, volume) VALUES (2, 1);
INSERT INTO sales (good_id, volume) VALUES (6, 2);
INSERT INTO sales (good_id, volume) VALUES (5, 2);
INSERT INTO sales (good_id, volume) VALUES (4, 2);
INSERT INTO sales (good_id, volume) VALUES (1, 1);
INSERT INTO sales (good_id, volume) VALUES (3, 1);
```

## Настройки

Для настроек генерации откройте файл `datagen.py` и изменяйте соответствующие параметры, которые находятся в начале файла.:

```
max_groups = 4                  # Максимальное количество груп, минимум 2.

group_max_goods = 3             # Максимальное количество типов товаров в одной группе.
    
good_max_price = 100            # Максимальная цена товара одного типа.
good_max_count = 100            # Максимальное количество товара одного типа.

min_sale_buy_ops = 1            # Минимальное количество операций покупки/продажи товара одного типа.
max_sale_buy_ops = 2            # Максимальное количество операций покупки/продажи товара одного типа.

min_goods_per_sale_buy = 10     # Минимальное количество товаров одного типа на операцию продажи/покупки.
max_goods_per_sale_buy = 20     # Максимальное количество товаров одного типа на операцию продажи/покупки.
```

## Примечания

Параметры `min_goods_per_sale_buy` и `max_goods_per_sale_buy` могут игнорироваться скриптом, при условии невозможности их выполнения.
Например если цена товара 100, максимальное количество операций 2 а максимальное количество товаров одного типа на операцию продажи/покупки 20. 
Т.е. 20 * 2 < 100, следовательно при таких параметрах невозможно выполнить данное условие.
