select
    (select ggroups.name from ggroups where id = group_id) as name
from(
    select
        *, (case when (volume_minus_sold  > volume) then volume else volume_minus_sold  end) as remained
    from(
        select 
            t.*,
            ((select 
                sum(volume) 
            
            from(
                acquisition as acq left join(
                                            select 
                                                sales.good_id, sum(sales.volume) as sold 
                                                
                                            from 
                                                sales 
                                                
                                                group by good_id) as sold_g on acq.good_id = sold_g.good_id) where acq.timestamp <= t.timestamp and acq.good_id = t.good_id group by acq.good_id) - sold) as volume_minus_sold 

        from(
            select 
                timestamp, acq.good_id, acq.volume, acq.price, ifnull(sold_g.sold, 0) as sold 
                
            from(
                acquisition as acq left join(
                                            select 
                                                sales.good_id, sum(sales.volume) as sold 
                                                
                                            from sales group by good_id) as sold_g on acq.good_id = sold_g.good_id)) as t) as t2 left join goods on goods.id = t2.good_id
    where volume_minus_sold  > 0) as t3
group by group_id order by sum(price * remained) desc limit 1;