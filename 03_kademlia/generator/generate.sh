#!/bin/bash

set -e

rm ./tests/* || true

python3 ./generator/generator.py 5 1 4 > ./tests/1
python3 ./generator/generator.py 5 1 4 > ./tests/2
python3 ./generator/generator.py 5 1 4 > ./tests/3
python3 ./generator/generator.py 5 1 4 > ./tests/4
python3 ./generator/generator.py 5 2 4 > ./tests/5
python3 ./generator/generator.py 5 2 4 > ./tests/6
python3 ./generator/generator.py 5 2 4 > ./tests/7
python3 ./generator/generator.py 20 1 8 > ./tests/8
python3 ./generator/generator.py 20 2 8 > ./tests/9
python3 ./generator/generator.py 20 3 8 > ./tests/10
python3 ./generator/generator.py 40 2 10 > ./tests/11
python3 ./generator/generator.py 40 3 11 > ./tests/12
python3 ./generator/generator.py 50 4 8 > ./tests/13
python3 ./generator/generator.py 100 2 16 > ./tests/14
python3 ./generator/generator.py 100 2 16 > ./tests/15
python3 ./generator/generator.py 100 3 16 > ./tests/16
python3 ./generator/generator.py 100 2 12 > ./tests/17
python3 ./generator/generator.py 100 3 13 > ./tests/18
python3 ./generator/generator.py 100 1 14 > ./tests/19
python3 ./generator/generator.py 150 3 16 > ./tests/20

for (( i = 1; i <= 20; i+=1 )); do
  fi=$(printf "%0d\n" $i)
  cat "./tests/$fi" | python3 solution/solution.py > "./tests/$fi.clue"
done

cp ./generator/exampleInput ./tests/0
cp ./generator/exampleOutput ./tests/0.clue