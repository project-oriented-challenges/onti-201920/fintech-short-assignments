import sys
import random

NODES = int(sys.argv[1])
K = int(sys.argv[2])
N = int(sys.argv[3])

print(f'{N} {K} {NODES}')

s = set()
while len(s) < NODES:
    s.add(random.randrange(2 ** N))
nodes = list(s)
nodes.sort()

times = [random.randrange(10000000) for i in range(NODES)]

for i in range(NODES):
    print(f'{nodes[i]} {random.randrange(10000000)}')
