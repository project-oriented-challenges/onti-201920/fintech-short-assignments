# This is a sample Code Challenge
# Learn more: https://stepik.org/lesson/9173
# Ask your questions via support@stepik.org

def generate():
    return []

def check(reply, clue):
    return reply.strip() == clue.strip()

# def solve(dataset):
#     a, b = dataset.split()
#     return str(int(a) + int(b))