def subtree_dist(x, y):
  return (x ^ y).bit_length()


class Node:
  def __init__(self, id):
    self.id = id
    self.routing_table = [[] for _ in range(N + 1)]

  def dist_to(self, node_id):
    q = [(self, 0)]
    while q[0][0].id != node_id:
      (node, cd) = q.pop(0)
      d = subtree_dist(node.id, node_id)
      for node1 in node.routing_table[d]:
        q.append((node1, cd + 1))
    return q[0][1]

  def add_node(self, node):
    d = subtree_dist(self.id, node.id)
    if len(self.routing_table[d]) < K:
      self.routing_table[d].append(node)


(N, K, NODES) = map(int, input().split())

v = []

for i in range(NODES):
  (id, time) = map(int, input().split())
  v.append((id, time))

v.sort(key=lambda x: x[1])

nodes = []
for i in range(NODES):
  nodes.append(Node(v[i][0]))

ans = (0, 0)
cur = (0, 0)
cur_diameter = 0

for i in range(len(v)):
  prev_diameter = cur_diameter
  for node in nodes[:i]:
    nodes[i].add_node(node)
    node.add_node(nodes[i])
  for node in nodes[:i + 1]:
    for node1 in nodes[:i + 1]:
      if cur_diameter < node.dist_to(node1.id):
        cur_diameter = node.dist_to(node1.id)
  if cur_diameter == prev_diameter:
    cur = (cur[0], i)
  else:
    cur = (i, i)
  if v[ans[1]][1] - v[ans[0]][1] < v[cur[1]][1] - v[cur[0]][1]:
    ans = cur
  #print(i, cur_diameter)

#print(ans)
print(nodes[ans[0]].id, nodes[ans[1]].id)
#print(v[ans[1]][1] - v[ans[0]][1])
